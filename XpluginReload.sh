#!/usr/bin/env bash -x

cordova platform rm android

cordova plugin rm cordova-plugin-whitelist
cordova plugin add cordova-plugin-whitelist

cordova plugin rm cordova-plugin-device
cordova plugin add cordova-plugin-device

cordova plugin rm cordova-plugin-device-orientation
cordova plugin add cordova-plugin-device-orientation

cordova plugin rm cordova-plugin-file
cordova plugin add cordova-plugin-file

cordova plugin rm cordova-plugin-device-motion
cordova plugin add cordova-plugin-device-motion

cordova plugin rm cordova-plugin-splashscreen
cordova plugin add cordova-plugin-splashscreen

cordova plugin rm cordova-plugin-dialogs
cordova plugin add cordova-plugin-dialogs

cordova plugin rm cordova-plugin-screen-orientation
cordova plugin add cordova-plugin-screen-orientation

cordova plugin rm cordova-plugin-spinner-dialog
cordova plugin add cordova-plugin-spinner-dialog

cordova plugin rm cordova-plugin-insomnia
cordova plugin add cordova-plugin-insomnia

cordova plugin rm cordova-sqlite-ext
cordova plugin add cordova-sqlite-ext

cordova plugin rm cordova-plugin-crosswalk-webview
#cordova plugin add cordova-plugin-crosswalk-webview

cordova plugin rm cordova-plugin-android-movetasktoback
cordova plugin add cordova-android-movetasktoback

cordova plugin rm cordova-plugin-inappbrowser
cordova plugin add cordova-plugin-inappbrowser

cordova plugin rm cordova-plugin-vibration
cordova plugin add cordova-plugin-vibration

cordova plugin rm ionic-plugin-keyboard
cordova plugin add ionic-plugin-keyboard

#cordova plugin rm com.flyingsoftgames.xapkreader
#cordova plugin add  https://github.com/agamemnus/cordova-plugin-xapkreader.git#cordova-5.3.1

cordova plugin rm cordova-plugin-x-socialsharing
cordova plugin add ./SocialSharing-PhoneGap-Plugin


cordova plugin add https://github.com/gitawego/cordova-screenshot.git

cordova platform add android
