-- ZDECISION

ALTER TABLE [ZDECISION] ADD COLUMN [main_rule_number] INTEGER;
ALTER TABLE [ZDECISION] ADD COLUMN [sub_rule_number] INTEGER;

UPDATE [ZDECISION]
SET [main_rule_number] = [ZMAINRULENUMBER];
UPDATE [ZDECISION]
SET [sub_rule_number] = [ZSUBRULENUM];

CREATE INDEX IF NOT EXISTS [zdecision_main_rule_number_key] ON [ZDECISION] ([main_rule_number], [sub_rule_number]);
CREATE INDEX IF NOT EXISTS [zdecision_sub_rule_number_key] ON [ZDECISION] ([sub_rule_number]);


-- ZDEFINITION

CREATE INDEX IF NOT EXISTS [zdefinition_ztitle_key] ON [ZDEFINITION] ([ZTITLE]);


-- ZMAINRULE

ALTER TABLE [ZMAINRULE] ADD COLUMN [main_rule_number] INTEGER;

UPDATE [ZMAINRULE]
SET [main_rule_number] = [ZRULENUM];

CREATE INDEX IF NOT EXISTS [zmainrule_rule_number_key] ON [ZMAINRULE] ([main_rule_number]);


-- ZSUBRULE

ALTER TABLE [ZSUBRULE] ADD COLUMN [main_rule_number] INTEGER;
ALTER TABLE [ZSUBRULE] ADD COLUMN [sub_rule_number] INTEGER;

UPDATE [ZSUBRULE]
SET [main_rule_number] = [ZMAINRULENUM];
UPDATE [ZSUBRULE]
SET [sub_rule_number] = [ZSUBRULENUM];

CREATE INDEX IF NOT EXISTS [zsubrule_main_rule_number_key] ON [ZSUBRULE] ([main_rule_number] ASC, [sub_rule_number]);
CREATE INDEX IF NOT EXISTS [zsubrule_sub_rule_number_key] ON [ZSUBRULE] ([sub_rule_number]);


VACUUM;
