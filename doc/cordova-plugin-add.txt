cordova plugin add cordova-plugin-whitelist --save
cordova plugin add cordova-plugin-device --save
cordova plugin add cordova-plugin-device-orientation --save
cordova plugin add cordova-plugin-file --save
cordova plugin add cordova-plugin-device-motion --save
cordova plugin add cordova-plugin-splashscreen --save
cordova plugin add cordova-plugin-dialogs --save
cordova plugin add cordova-plugin-screen-orientation --save
cordova plugin add cordova-plugin-spinner-dialog --save
cordova plugin add cordova-plugin-insomnia --save
cordova plugin add cordova-sqlite-ext --save
cordova plugin add cordova-android-movetasktoback --save
cordova plugin add cordova-plugin-crosswalk-webview --save
cordova plugin add cordova-plugin-inappbrowser --save
cordova plugin add cordova-plugin-vibration --save
cordova plugin add ionic-plugin-keyboard --save
cordova plugin add cordova-plugin-x-socialsharing --save
cordova plugin add https://github.com/agamemnus/cordova-plugin-xapkreader.git#cordova-5.3.1 --save
