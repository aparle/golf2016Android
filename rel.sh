#!/usr/bin/env bash -x
# 02073712119


cordova build  --release android
cp ./platforms/android/build/outputs/apk/android-release-unsigned.apk ./platforms/android/build/outputs/apk/golf2016-release-unsigned.apk
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore /Code/AndroidKeyStore/my-release-key.keystore ./platforms/android/build/outputs/apk/android-release-unsigned.apk  alias_name
rm ./platforms/android/build/outputs/apk/golf2016-signed-aligned.apk
zipalign -v 4 ./platforms/android/build/outputs/apk/android-release-unsigned.apk ./platforms/android/build/outputs/apk/golf2016-signed-aligned.apk
aapt dump badging ./platforms/android/build/outputs/apk/golf2016-signed-aligned.apk | grep versionCode=

echo "version number:"
read version
mv  ./platforms/android/build/outputs/apk/golf2016-signed-aligned.apk ./platforms/android/build/outputs/apk/golf2016-signed-aligned.$version.apk
mv ./platforms/android/build/outputs/apk/golf2016-release-unsigned.apk.apk ./platforms/android/build/outputs/apk/golf2016-release-unsigned.apk.$version.apk
echo "./platforms/android/build/outputs/apk/golf2016-signed-aligned.$version.apk"
adb uninstall com.aimermedia.randa.golf2016
aapt dump badging ./platforms/android/build/outputs/apk/golf2016-signed-aligned.$version.apk | grep versionCode=

adb install ./platforms/android/build/outputs/apk/golf2016-signed-aligned.$version.apk
adb shell am start -n com.aimermedia.randa.golf2016/.MainActivity
