window.useSearch = true;
window.useVideos = true;
window.useDummyVideo = false;
window.useVideoTags = true; //if false, we shall use iframe
window.cleanVideoFileNames = false;
window.firstStart = true;
window.foldersData = {};
window.foldersDataFileName = "my_decisions.json";
window.rulesSequence = [];
window.rulesAndSubRulesSequence = [];
window.searchSequence = [];
window.decisionsSequence = [];
window.cacheMainRules = {};
window.cacheSubRules = {};
window.cacheDecisions = {};
window.sharingSubject = "";
window.sharingMessage = "";
window.historyBackDetected = false;
window.lateralMovementDetected = false;
window.menuTransitionDuration = 333;
window.addToFolderTransitionDuration = 333;
(function ($) {
    "use strict";
    //main module
    app.main = function () {
        var mask,
            pages,
            rdlv,
            rlv,
            srlv,
            fv,
            frdlv,
            vlv,
            rv,
            dv,
            dfv,
            dfsv,
            av,
            pv,
            ov,
            hv,
            sv,
            mv,
            mvc,
            atfv,
            atfvc,
            edv,
            rxv,
            initViewHolders = function () {
                mask = $("#mask");
                pages = $("#pages");
                rdlv = $("#rules-and-decisions-list-view");
                rlv = $("#rules-list-view");
                srlv = $("#sub-rules-list-view");
                fv = $("#folders-list-view");
                frdlv = $("#folder-rules-and-decisions-list-view");
                vlv = $("#videos-list-view");
                rv = $("#rule-view");
                dv = $("#decision-view");
                dfv = $("#definition-view");
                dfsv = $("#definitions-view");
                av = $("#appendix-view");
                pv = $("#page-view");
                ov = $("#overview-view");
                hv = $("#header-view");
                sv = $("#search-view");
                mv = $("#menu-view");
                mvc = $("#menu-view-close");
                atfv = $("#add-to-folder-view");
                atfvc = $("#add-to-folder-view-close");
                edv = $("#explore-decisions-view");
                rxv = $("#rolex-view");
            },
        //router
            _historyStack = [],
            _currentView = null,
            Router = Backbone.Router.extend({
                routes: {
                    "": "rulesAndDecisionsList",
                    "rulesAndDecisionsList": "rulesAndDecisionsList",
                    "rulesList": "rulesList",
                    "subRulesList/:mainRuleId": "subRulesList",
                    "folders": "foldersList",
                    "folder/:folderId": "folderRulesAndDecisionsList",
                    "videosList": "videosList",
                    "rule/:rute": "rule",
                    "decision/:rute": "decision",
                    "definitions": "definitions",
                    "appendix/:id": "appendix",
                    "page/:fileName": "page",
                    "overview": "overview",
                    "etiquette": "etiquette",
                    "rolex": "rolex"
                },
                execute: function (callback, args) {
                    logger(window.location.hash, "location.hash");
                    addRouteToHistoryStack();
                    if (window.location.hash.replace(/^#/, "") === "") {//redirect empty route
                        app.router.navigateCustom("#rulesAndDecisionsList", {trigger: true, replace: true});
                    }
                    if (window.exploreDecisionsDisplayed) {
                        hideExploreDecisions();
                    }
                    if (window.searchDisplayed) {
                        hideSearch();
                    }
                    if (window.useNativeAnimation) {
                        var options = {
                            direction: (window.historyBackDetected ? "right" : "left")
                        };
                        if (window.lateralMovementDetected) {
                            options.duration = 167;//quick slide
                            window.lateralMovementDetected = false;
                        }
                        window.plugins["nativepagetransitions"]["slide"](options);
                    }
                    hv.html((new app.HeaderView()).render().$el);
                    if (typeof callback === "function") {
                        callback.apply(this, args);
                    }
                },
                _initAnimation: function (view) {
                    if (window.useAnimation) {
                        if (window.historyBackDetected) {
                            view.$el.addClass("swipe-right-start");
                        } else {
                            view.$el.addClass("swipe-left-start");
                        }
                        mask.addClass("mask-displayed");
                    }
                },
                _goTo: function (view) {
                    logger(window.historyBackDetected, "_goTo, historyBackDetected", true);
                    var previous = _currentView,
                        previousParent,
                        previousParentId,
                        next = view,
                        nextParent = next.$el.parent(),
                        nextParentId = nextParent.attr("id"),
                        isListView,
                        removePreviousView;
                    _currentView = next;
                    if (previous) {
                        previousParent = previous.$el.parent();
                        previousParentId = previousParent.attr("id");
                        isListView = previousParent.hasClass("list-view");
                        removePreviousView = function () {
                            if (previousParentId !== nextParentId) {//don't hide wrappers in case of lateral movements
                                previousParent.hide();
                            }
                            if (!isListView) {//don't remove list views
                                if ($.isFunction(previous._removeIScroll)) {
                                    previous._removeIScroll();
                                }
                                if ($.isFunction(previous.remove)) {
                                    previous.remove();
                                } else {
                                    previous.$el.remove();
                                }
                            }
                            if (window.useAnimation) {
                                mask.removeClass("mask-displayed");
                                next.$el.removeClass("swipe-right-start swipe-left-start swipe-right-open swipe-left-open");
                            }
                        };
                        if (window.useAnimation && !window.useNativeAnimation) {
                            if (!isListView && typeof previous.undelegateEvents === "function") {
                                previous.undelegateEvents();
                            }
                            //previous.animateOut(removePreviousView, window.historyBackDetected);
                            /*
                             if (window.historyBackDetected) {
                             previousParent.addClass("swipe-right-close");
                             } else {
                             previousParent.addClass("swipe-left-close");
                             }
                             //*/
                            _.delay(removePreviousView, 500);
                        } else {
                            removePreviousView();
                        }
                    }
                    _.delay(function () {
                        nextParent.show();
                        _.delay(function () {
                            if (previous && isListView && previousParentId === nextParentId) {
                                //don't make transition between the list pages going to themselves
                                if (window.useNativeAnimation) {
                                    window.plugins["nativepagetransitions"]["cancelPendingTransition"]();
                                }
                            } else {
                                if (window.useNativeAnimation) {
                                    if (window.firstStart) {
                                        window.plugins["nativepagetransitions"]["cancelPendingTransition"]();
                                    } else {
                                        window.plugins["nativepagetransitions"]["executePendingTransition"]();
                                    }
                                    setDefaultOptions();//reset any temporary changes
                                } else if (window.useAnimation) {
                                    //next.animateIn(null, window.historyBackDetected);
                                    if (window.historyBackDetected) {
                                        nextParent.addClass("swipe-right-open");
                                    } else {
                                        nextParent.addClass("swipe-left-open");
                                    }
                                }
                            }
                            window.historyBackDetected = false;
                        }, 100);//wait for the painting of the new view
                    }, 100);//wait for the removal of the previous view before triggering the transition
                    /*
                     logger(previous, "previous");
                     logger(previousParentId, "previousParentId");
                     logger(isListView, "isListView");
                     logger(nextParentId, "nextParentId");
                     //*/
                },
                _showView: function (view, container) {
                    var self = this,
                        views;
                    view.callback = function () {
                        self._goTo(view);
                    };
                    self._initAnimation(container);
                    container.prepend(view.render().$el).show();//first display the new view
                    views = container.children();
                    if (views.length > 1) {
                        views.last().remove();//then remove the old one, if any
                    }
                },
                initialize: function () {
                    logger("router: initialize");
                },
                rulesAndDecisionsList: function () {
                    var self = this;
                    if (!window.firstStart && !window.useNativeAnimation && window.numberOfCPUs !== 0 && window.numberOfCPUs < 4) {
                        spinnerDialogShow();
                    }
                    self._initAnimation(rdlv);
                    if (app.rulesAndDecisionsListView === undefined) {
                        app.rulesAndDecisionsListView = new app.RulesAndDecisionsListView();
                        app.rulesAndDecisionsListView.callback = function () {
                            self._goTo(app.rulesAndDecisionsListView);
                            if (window.exploreDecisionsOn && !window.exploreDecisionsDisplayed) {
                                _.delay(showExploreDecisions, 200);
                            }
                            if (window.searchOn && !window.searchDisplayed) {
                                _.delay(showSearch, 200);
                            }
                        };
                        rdlv.html(app.rulesAndDecisionsListView.render().$el);
                    } else {
                        logger("re-visiting rulesAndDecisionsList");
                        //when not building the view from scratch, we still need to deal with the following
                        _.delay(function () {
                            _calculateHeight();
                            app.rulesAndDecisionsListView.callback();
                            app.rulesAndDecisionsListView.onOrientationChange();
                            highlightListItemByRetrieval();
                        }, 150);//wait for drawing of the view
                    }
                    rdlv.show();
                },
                rulesList: function () {
                    var self = this;
                    self._initAnimation(rlv);
                    if (app.rulesListView === undefined) {
                        app.rulesListView = new app.RulesListView();
                        app.rulesListView.callback = function () {
                            self._goTo(app.rulesListView);
                        };
                        rlv.html(app.rulesListView.render().$el);
                    } else {
                        //when not drawing the view, we still need to deal with the following
                        _.delay(function () {
                            app.rulesListView.callback();
                            app.rulesListView.onOrientationChange();
                            highlightListItemByRetrieval();
                        }, 150);//wait for drawing of the view
                    }
                    rlv.show();
                },
                subRulesList: function (mainRuleId) {
                    var view = new app.SubRulesListView();
                    view.mainRuleId = mainRuleId;
                    this._showView(view, srlv);
                },
                foldersList: function () {
                    this._showView((new app.FoldersListView()), fv);
                },
                folderRulesAndDecisionsList: function (folderId) {
                    var view = new app.FolderRulesAndDecisionsListView();
                    view.folderId = folderId;
                    this._showView(view, frdlv);
                },
                videosList: function () {
                    var self = this;
                    self._initAnimation(vlv);
                    if (app.videosListView === undefined) {
                        app.videosListView = new app.VideosListView();
                        app.videosListView.callback = function () {
                            self._goTo(app.videosListView);
                        };
                        vlv.html(app.videosListView.render().$el);
                    } else {
                        //when not drawing the view, we still need to deal with the following
                        _.delay(function () {
                            app.videosListView.callback();
                            app.videosListView.onOrientationChange();
                            highlightListItemByRetrieval();
                        }, 150);//wait for drawing of the view
                    }
                    vlv.show();
                },
                rule: function (rute) {
                    var view = new app.RuleView();
                    view.rute = rute;
                    this._showView(view, rv);
                },
                decision: function (rute) {
                    var view = new app.DecisionView();
                    view.rute = rute;
                    this._showView(view, dv);
                },
                definitions: function () {
                    this._showView((new app.DefinitionsView()), dfsv);
                },
                appendix: function (id) {
                    var view = new app.AppendixView();
                    view.appendixId = id;
                    this._showView(view, av);
                },
                page: function (fileName) {
                    var view = new app.PageView();
                    view.fileName = fileName;
                    this._showView(view, pv);
                },
                overview: function () {
                    this._showView((new app.OverviewView()), ov);
                },
                etiquette: function () {
                    app.router.navigateCustom("#page/Etiquette", {trigger: true, replace: true});
                },
                rolex: function () {
                    this._showView((new app.RolexView()), rxv);
                }
            }),
        //history stack manipulation
            _getRouteKind = function (route) {
                if (!route || typeof route !== "string") {
                    logger(route, "_getRouteKind, incorrect route supplied", true);
                    return "";
                }
                return route.split("/").shift().replace(/^#/, "");
            },
            addRouteToHistoryStack = function (route) {
                route = route || Backbone.history.getFragment();
                //logger(route, "addRouteToHistoryStack, route", true);
                if (getRouteFromHistoryStack() === route) {
                    //don't add the same route as the last one
                    logger("addRouteToHistoryStack, same route, not adding");
                } else {
                    var idx = findLastOfTheKindInHistoryStack(route);
                    if (idx !== false && idx >= 0 && _historyStack[idx].route === route) {
                        logger("addRouteToHistoryStack, same route, reversing to history");
                        //reverse to the recently requested same route, but don't delete it!
                        goBackInHistoryStack(_historyStack.length - (idx + 1));
                    } else {
                        logger("addRouteToHistoryStack, new route, adding to history");
                        window.historyBackDetected = false;//make sure there is no unfinished backward movement
                        _historyStack.push({
                            route: route,
                            kind: _getRouteKind(route)
                        });
                    }
                }
                //logger(_historyStack, "addRouteToHistoryStack, _historyStack");
            },
            replaceRouteInHistoryStack = function (route) {
                if (!route || typeof route !== "string") {
                    logger(route, "replaceRouteInHistoryStack, incorrect route supplied", true);
                    return;
                }
                if (_historyStack.length > 0) {
                    _historyStack.pop();
                }
                _historyStack.push({
                    route: route,
                    kind: _getRouteKind(route)
                });
            },
            getObjectFromHistoryStack = function (countOfStepsBackwards) {
                countOfStepsBackwards = countOfStepsBackwards || 0;
                var index = _historyStack.length - (countOfStepsBackwards + 1);
                return (index >= 0 && $.isPlainObject(_historyStack[index]))
                    ? _historyStack[index]
                    : false;
            },
            getPropertyFromHistoryStack = function (countOfStepsBackwards, propertyName) {
                var obj = getObjectFromHistoryStack(countOfStepsBackwards);
                //logger(obj, "getPropertyFromHistoryStack, obj");
                return ($.isPlainObject(obj) && obj.hasOwnProperty(propertyName))
                    ? obj[propertyName]
                    : false;
            },
            getRouteFromHistoryStack = function (countOfStepsBackwards) {
                return getPropertyFromHistoryStack(countOfStepsBackwards, "route");
            },
            getKindFromHistoryStack = function (countOfStepsBackwards) {
                return getPropertyFromHistoryStack(countOfStepsBackwards, "kind");
            },
            getHighlightedElementFromHistoryStack = function (countOfStepsBackwards) {
                return getPropertyFromHistoryStack(countOfStepsBackwards, "highlight");
            },
            addHighlightedElementToHistoryStack = function (elId) {
                var lastIndex = _historyStack.length - 1;
                if (lastIndex >= 0 && $.isPlainObject(_historyStack[lastIndex])) {
                    _historyStack[lastIndex].highlight = elId;
                    //logger(_historyStack, "addHighlightedElementToHistoryStack, _historyStack");
                }
            },
            findLastOfTheKindInHistoryStack = function (route) {
                if (!route || typeof route !== "string") {
                    logger(route, "findLastOfTheKindInHistoryStack, incorrect route supplied", true);
                    return -1;
                }
                var idx = _.findLastIndex(_historyStack, {
                    kind: _getRouteKind(route)
                });
                logger(idx, "findLastOfTheKindInHistoryStack, idx");
                if (idx >= 0) {
                    logger(_historyStack[idx].route, "findLastOfTheKindInHistoryStack, route");
                }
                return idx;
            },
            goBackInHistoryStack = _.throttle(function (countOfStepsBackwards, usingNativeBackButton) {
                var out;
                //logger(countOfStepsBackwards, "goBackInHistoryStack, (A) reversing this many steps");
                if ((countOfStepsBackwards === undefined || countOfStepsBackwards === null) && _historyStack.length === 1) {
                    //don't try to reverse into the space before the initial page
                    if (usingNativeBackButton) {
                        navigator.notification.confirm("Exit the app?", function (button) {
                            if (button === 1) {
                                if (/Android/i.test(navigator.userAgent)) {
                                    if (typeof mayflower !== "undefined" && $.isFunction(mayflower.moveTaskToBack)) {
                                        mayflower.moveTaskToBack();
                                    } else if ($.isFunction(navigator.app.exitApp)) {
                                        navigator.app.exitApp();
                                    }
                                }
                            }
                        });
                        return null;
                    }
                } else {
                    window.historyBackDetected = true;
                    countOfStepsBackwards = countOfStepsBackwards || 1;
                    if ((_historyStack.length - countOfStepsBackwards) < 1) {
                        //adjust the count to never go beyond the initial page
                        countOfStepsBackwards = _historyStack.length - 1;
                    }
                    for (var i = 0; i < countOfStepsBackwards; i++) {
                        _historyStack.pop();//remove all but the one where we need to go
                    }
                    //logger(countOfStepsBackwards, "goBackInHistoryStack, (B) reversing this many steps");
                    //we can't use the following line as there is some kind of miscalculation going on
                    //perhaps related to the movement within the stack *before* actually landing on the requested page
                    //window.history.go(parseInt("-" + (countOfStepsBackwards + 1).toString()));
                    //.
                    //proceed to the requested page by reversal
                    window.location.hash = "#" + _historyStack[_historyStack.length - 1].route;
                }
                out = _historyStack[_historyStack.length - 1];
                //logger(out, "goBackInHistoryStack, reversing to this item");
                return out;//return the item we need to visit
            }, 1000, true),
        //get the rules' sequence - main-rules with their sub-rules - (async)
            getAndCacheTheRuleSequence = function () {
                var mainRules,
                    rules = [],
                    rules2 = [];
                queryAndProcessSqlResult(
                    "SELECT [main_rule_number] FROM [ZMAINRULE] ORDER BY [main_rule_number]",
                    {}, null,
                    function (res) {
                        mainRules = res;
                        _.each(res, function (value, key) {
                            rules[key] = value["main_rule_number"].toString();
                        });
                        window.rulesSequence = app.core.iterifyArr(rules);
                        queryAndProcessSqlResult(
                            "SELECT [ZRUTE] FROM [ZSUBRULE] ORDER BY [main_rule_number], [sub_rule_number]",
                            {}, null,
                            function (res) {
                                _.each(mainRules.concat(res), function (value, key) {
                                    rules2[key] = (value["main_rule_number"] || value["ZRUTE"]).toString();
                                });
                                window.rulesAndSubRulesSequence = app.core.iterifyArr(rules2.sort(function (a, b) {
                                    return parseFloat(a.replace("-", ".")) - parseFloat(b.replace("-", "."));
                                }));//sort rute strings as numbers
                                //console.dir(window.rulesAndSubRulesSequence);
                                /*
                                 var obj = {};
                                 _.each(window.rulesAndSubRulesSequence, function (e, i) {
                                 obj[i] = makeSortableDecisionId(e);
                                 });
                                 console.dir(obj);
                                 //*/
                            }
                        );
                    }
                );
            },
        //get the decisions' sequence - (async)
            getAndCacheTheDecisionSequence = function (callback) {
                var decisions = [];
                queryAndProcessSqlResult(
                    "SELECT [ZRUTE] FROM [ZDECISION] ORDER BY [ZORDER]",
                    {}, null,
                    function (res) {
                        _.each(res, function (value, key) {
                            decisions[key] = value["ZRUTE"];
                        });
                        window.decisionsSequence = app.core.iterifyArr(decisions);
                        if ($.isFunction(callback)) {
                            callback();
                        }
                        //console.dir(window.decisionsSequence);
                        /*
                         var obj = {};
                         _.each(window.decisionsSequence, function (e, i) {
                         obj[i] = makeSortableDecisionId(e);
                         });
                         console.dir(obj);
                         //*/
                    }
                );
            },
        //get the search sequence - (async)
            getAndCacheTheSearchSequence = function () {
                var names = [];
                queryAndProcessSqlResult(
                    "SELECT [ZNAME] FROM [ZSEARCHTERM] ORDER BY [ZNAME]",
                    {}, null,
                    function (res) {
                        _.each(res, function (value, key) {
                            names[key] = value["ZNAME"];
                        });
                        window.searchSequence = app.core.iterifyArr(names);
                    }
                );
            },
        //miscellaneous functions
            removeLettersAfterTheSubRuleDigits = function (txt) {
                if (typeof txt !== "string" || txt.length === 0) {
                    return txt || "";
                }
                return txt.replace(/[a-z]+$/ig, "");
            },
            makeDecisionIdFromNumberText = function (txt) {
                if (typeof txt !== "string" || txt.length === 0) {
                    return txt || "";
                }
                return txt.replace("/", "__").replace(".", "_");
            },
            makeSortableDecisionId = function (txt) {
                if (typeof txt !== "string" || txt.length === 0) {
                    return txt || "";
                }
                txt = txt.replace("M", "35");
                var prefix = "_",
                    arr,
                    beforeHyphen,
                    afterHyphen,
                    beforeSlash,
                    afterSlash,
                    beforeDot,
                    afterDot;
                //extract parts
                arr = txt.split(".");
                beforeDot = arr[0] || "";
                afterDot = arr[1] || "";
                arr = beforeDot.split("/");
                beforeSlash = arr[0] || "";
                afterSlash = arr[1] || "";
                arr = beforeSlash.split("-");
                beforeHyphen = arr[0] || "";
                afterHyphen = arr[1] || "";
                //fix the parts
                if (beforeHyphen.length === 1) {
                    beforeHyphen = "0".concat(beforeHyphen);
                }
                if (afterHyphen.length === 0) {
                    afterHyphen = "00 ";
                } else if (afterHyphen.length === 1) {
                    afterHyphen = "0".concat(afterHyphen, " ");
                } else if (/[a-z]$/.test(afterHyphen)) {
                    var len = afterHyphen.length,
                        digits = afterHyphen.slice(0, len - 1),
                        letter = afterHyphen.slice(-1);
                    afterHyphen = (len === 2)
                        ? "0".concat(digits, letter)
                        : digits.concat(letter);
                } else {//two digits
                    afterHyphen = afterHyphen.concat(" ");
                }
                if (afterSlash.length === 0) {
                    afterSlash = "00";
                } else if (afterSlash.length === 1) {
                    afterSlash = "0".concat(afterSlash);
                }
                if (afterDot.length === 0) {
                    afterDot = "00";
                } else if (afterDot.length === 1) {
                    afterDot = "0".concat(afterDot);
                }
                return prefix.concat(beforeHyphen, afterHyphen, afterSlash, afterDot);
            },
            convertAppendixImages = function (txt) {
                if (typeof txt !== "string" || txt.length === 0) {
                    return txt || "";
                }
                txt = txt.replace(/<img .*?src='\/sitecore\/shell\/~\/media\/([^\.]+\.ashx)(?:\?la=en)?'.*?\/>/ig, "<img class='image mfp-image' src='img/others/$1.jpeg' data-mfp-src='img/others/$1.jpeg' title=''/>");
                return txt;
            },
            convertDecisionImage = function (txt, imageName, thumbName, title) {
                if (typeof txt !== "string" || txt.length === 0) {
                    return txt || "";
                }
                title = title.replace(/["']/g, "");
                txt = txt.replace(/<img .*?media.*?\/>/ig, "<img class='image mfp-image' src='img/media/" + thumbName + "' data-mfp-src='img/media/" + imageName + "' title='" + (title || "") + "'/>");
                return txt;
            },
            convertLinks = function (txt) {
                if (typeof txt !== "string" || txt.length === 0) {
                    return txt || "";
                }
                //sub-rules, old version
                //txt = txt.replace(/<a .*?href='https?:\/\/.*?randa\.org\/Rules-of-Golf\/MainRules\/(\d+)-[^\/]+\/SubRules\/(\d+)-[^\/]+'>(.*?)<\/a>/ig, "<a href='#' class='link-to-rule' data-rule='$1-$2'>$3</a>");
                //rules
                txt = txt.replace(/<a\s+?href=['"]rule:(\d+(?:[-–]\d+[a-z]*)?)['"]>(.*?)<\/a>/ig, "<a href='#' class='link-to-rule needsclick' data-rule='$1'>$2</a>");
                //appendices
                txt = txt.replace(/<a\s+?href=['"]rule:appendix:([^'"]+?)['"]>(.*?)<\/a>/ig, "<a href='#' class='link-to-appendix needsclick' data-appendix='$1'>$2</a>");
                //decisions
                txt = txt.replace(/<a\s+?href=['"]decision:([^'"]+?)['"]>(.*?)<\/a>/ig, "<a href='#' class='link-to-decision needsclick' data-decision='$1'>$2</a>");
                //sitecore images
                txt = txt.replace(/<img .*?src='\/sitecore\/shell\/~\/media\/([^\.]+\.ashx)(?:\?la=en)?'.*?\/>/ig, "<img src='img/others/$1.jpeg'/>");
                //penalty class, for improved search
                txt = txt.replace(/(='penalty)/ig, "='pnlt");
                return txt;
            },
            convertMediaLinks = function (txt) {
                if (typeof txt !== "string" || txt.length === 0) {
                    return txt || "";
                }
                //rules
                txt = txt.replace(/([ (])Rule\s+([0-9a-z-–]+)([ ),.])/ig, "$1<a href='#' class='link-to-rule needsclick' data-rule='$2'>Rule $2</a>$3");
                //decisions
                txt = txt.replace(/([ (])Decision\s+([0-9a-z-–\/]+)([ ),.])/ig, "$1<a href='#' class='link-to-decision needsclick' data-decision='$2'>Decision $2</a>$3");
                return txt;
            },
            highlightListItemByAddition = function (el, callback) {
                var id = getHighlightedElementFromHistoryStack();
                if (id) {
                    var el2 = $("#" + id);
                    //logger(el2, "highlightListItemByAddition, el to remove hl");
                    if (el2.length) {
                        el2.removeClass("hl");
                    }
                }
                if (window.location.hash.indexOf("rulesAndDe") === -1) {//avoid the large home page
                    el.parent().find(".hl").removeClass("hl");//remove all that occur
                }
                el.addClass("hl");
                addHighlightedElementToHistoryStack(el.attr("id"));
                if (_.isFunction(callback)) {
                    callback();
                }
            },
            highlightListItemByRetrieval = function (countOfStepsBackwards) {
                var id = getHighlightedElementFromHistoryStack(countOfStepsBackwards);
                //logger(id, "highlightListItemByRetrieval, id");
                if (id) {
                    var el = $("#" + id);
                    //logger(el, "highlightListItemByRetrieval, el");
                    if (el.length) {
                        el.addClass("hl");
                    }
                }
            },
            replaceImgBySvg = function (el) {
                /*
                 if (el.length < 1) {
                 el = $(document);
                 }
                 el.find("img").each(function () {
                 var img = $(this),
                 attr = img.prop("attributes"),
                 svg;
                 $.get(img.attr("src"), function (data) {
                 svg = $(data).find('svg').removeAttr('xmlns:a');//remove any invalid XML tags
                 $.each(attr, function (ignore, e) {//loop through IMG attributes and apply on SVG
                 svg.attr(e.name, e.value);
                 });
                 img.replaceWith(svg);
                 }, 'xml');
                 });
                 //*/
            },
            setTemporaryOptionsForLateralMovements = function () {
                if (window.useNativeAnimation) {
                    window.plugins["nativepagetransitions"]["globalOptions"].fixedPixelsBottom = 44;
                }
            },
            toggleHolder = function (view, holderButton, listItems) {
                var svg = holderButton.find("svg");
                if (svg.length < 1) {
                    svg = holderButton.find("img");
                }
                if (listItems.hasClass("close")) {
                    svg.removeClass("close").addClass("open");
                    listItems.removeClass("close").addClass("open");
                } else {
                    svg.removeClass("open").addClass("close");
                    listItems.removeClass("open").addClass("close");
                }
                holderButton.addClass("clicked");
                _.delay(function () {
                    holderButton.removeClass("clicked");
                }, 200);
                view.onOrientationChange();
            },
            highlightClickedButton = function (el) {
                if (el && typeof el === "string") {
                    el = $(el);
                }
                el.addClass("clicked");
                _.delay(function () {
                    if (el.length > 0) {
                        el.removeClass("clicked");
                    }
                }, 1000);
            },
        //menu
            showMenu = _.debounce(function () {
                if (window.menuStatus === "opening" || window.menuStatus === "closing") {
                    return;
                }
                if (window.menuStatus === "opened") {
                    hideMenu();
                    return;
                }
                logger("show menu");
                window.menuStatus = "opening";
                if (window.useNativeAnimation && window.useNativeMenuAnimation) {
                    window.plugins["nativepagetransitions"]["drawer"]({
                        origin: "left",
                        action: "open"
                    });
                }
                var prepare = function () {
                    app.menuView = new app.MenuView();
                    app.menuView.callback = function () {
                        //_currentView.$el.hide();
                        if ($.isFunction(mvc.hammer)) {
                            mvc.hammer();
                        }
                        mvc.show();
                    };
                    app.menuView.render();
                    _.delay(function () {
                        mv.html(app.menuView.$el).show();
                        _.delay(function () {
                            app.menuView.$el.addClass("show");
                        }, 50);
                    }, 50);
                };
                _.delay(prepare, 150);
            }, 300, true),
            hideMenu = _.debounce(function () {
                if (window.menuStatus === "opening" || window.menuStatus === "closing" || window.menuStatus === "closed") {
                    return;
                }
                logger("hide menu");
                window.menuStatus = "closing";
                mvc.hide();
                if (window.useNativeAnimation && window.useNativeMenuAnimation) {
                    window.plugins["nativepagetransitions"]["drawer"]({
                        origin: "left",
                        action: "close"
                    });
                }
                var finalise = function () {
                        window.menuStatus = "closed";
                    },
                    execute = function () {
                        window.plugins["nativepagetransitions"]["executePendingTransition"]();
                        _.delay(finalise, window.menuTransitionDuration + 50);
                    },
                    prepare = function () {
                        if (window.useNativeAnimation && window.useNativeMenuAnimation) {
                            if ($.isFunction(app.menuView.remove)) {
                                app.menuView.remove();
                            }
                            mv.hide().empty();
                            //_currentView.$el.show();
                            _.delay(execute, 300);
                        } else {
                            mv.find("#menu-view-i").removeClass("show").addClass("hide");
                            _.delay(function () {
                                if ($.isFunction(app.menuView.remove)) {
                                    app.menuView.remove();
                                }
                                mv.hide().empty();
                                finalise();
                            }, window.menuTransitionDuration + 50);
                        }
                    };
                _.delay(prepare, 150);
            }, 300, true),
        //folders
            showAddToFolder = _.debounce(function () {
                if (window.addToFolderStatus === "opening" || window.addToFolderStatus === "closing") {
                    return;
                }
                if (window.addToFolderStatus === "opened") {
                    hideMenu();
                    return;
                }
                logger("show addToFolder");
                window.addToFolderStatus = "opening";
                if (window.useNativeAnimation && window.useNativeMenuAnimation) {
                    window.plugins["nativepagetransitions"]["drawer"]({
                        origin: "right",
                        action: "open"
                    });
                }
                var prepare = function () {
                    app.addToFolderView = new app.AddToFolderView();
                    app.addToFolderView.callback = function () {
                        //_currentView.$el.hide();
                        if ($.isFunction(atfvc.hammer)) {
                            atfvc.hammer();
                        }
                        atfvc.show();
                    };
                    app.addToFolderView.render();
                    _.delay(function () {
                        atfv.html(app.addToFolderView.$el).show();
                        _.delay(function () {
                            app.addToFolderView.$el.addClass("show");
                        }, 50);
                    }, 50);
                };
                _.delay(prepare, 150);
            }, 300, true),
            hideAddToFolder = _.debounce(function () {
                if (window.addToFolderStatus === "opening" || window.addToFolderStatus === "closing" || window.addToFolderStatus === "closed") {
                    return;
                }
                logger("hide addToFolder");
                window.addToFolderStatus = "closing";
                atfvc.hide();
                if (window.useNativeAnimation && window.useNativeMenuAnimation) {
                    window.plugins["nativepagetransitions"]["drawer"]({
                        origin: "right",
                        action: "close"
                    });
                }
                var finalise = function () {
                        window.addToFolderStatus = "closed";
                    },
                    execute = function () {
                        window.plugins["nativepagetransitions"]["executePendingTransition"]();
                        _.delay(finalise, window.addToFolderTransitionDuration + 50);
                    },
                    prepare = function () {
                        if (window.useNativeAnimation && window.useNativeMenuAnimation) {
                            if ($.isFunction(app.addToFolderView.remove)) {
                                app.addToFolderView.remove();
                            }
                            atfv.hide().empty();
                            //_currentView.$el.show();
                            _.delay(execute, 300);
                        } else {
                            atfv.find("#add-to-folder-view-i").removeClass("show").addClass("hide");
                            _.delay(function () {
                                if ($.isFunction(app.addToFolderView.remove)) {
                                    app.addToFolderView.remove();
                                }
                                atfv.hide().empty();
                                finalise();
                            }, window.addToFolderTransitionDuration + 50);
                        }
                    };
                _.delay(prepare, 150);
            }, 300, true),
        //explore decisions
            mbsInst,
            createExploreDecisions = function () {
                var id = "explore-list",
                    el,
                    lastRule,
                    lastSubRule,
                    arr,
                    arr2,
                    n1,
                    n2,
                    n3,
                    level1 = $("<ul/>").attr("id", id),
                    level2,
                    level3,
                    li;
                _.each(window.decisionsSequence, function (decision) {
                    if (decision.indexOf("-") === -1) {//main rule's decision
                        arr = decision.split("/", 2);
                        n1 = arr[0];
                        n2 = "/";
                        n3 = arr[1] || "";
                    } else {//sub rule's decision
                        arr = decision.split("-", 2);
                        n1 = arr[0];
                        arr2 = arr[1].split("/", 2);
                        n2 = arr2[0];
                        n3 = arr2[1] || "";
                    }
                    if (n1 !== lastRule) {
                        level2 = $("<ul/>");
                        li = $("<li/>").attr("data-val", n1).text(n1).append(level2);
                        level1.append(li);//add the new main rule
                        lastRule = n1;
                        lastSubRule = null;//reset the sub-rule, in order to prevent accidental usage of sub-rule (level2) of the previous main rule (this happened when going from rule 21 to rule 22 (both don't have any sub-rules, so the lastSubRule was "/" in both cases)
                    }
                    if (n2 !== lastSubRule) {
                        level3 = $("<ul/>");
                        li = $("<li/>").attr("data-val", n2).text(n2).append(level3);
                        level2.append(li);//add the new sub rule
                        lastSubRule = n2;
                    }
                    li = $("<li/>").attr("data-val", n3).text(n3);
                    level3.append(li);//add the new decision
                });
                edv.append(level1);
                el = edv.find("#" + id);
                el.mobiscroll().treelist({
                    buttons: [],
                    display: "bottom",
                    focusTrap: false,
                    labels: ["Main Rule", "Sub Rule", "Decision"],
                    showInput: false,
                    showLabel: true,
                    theme: "ios-golf",
                    formatValue: function (data) {
                        return (data[1] === "/")
                            ? data.join("")
                            : data[0] + "-" + data[1] + "/" + data[2];
                    },
                    onClosed: function () {
                        hideExploreDecisions();
                    },
                    onChange: function (valueText) {
                        var decId = "rdl-d" + makeDecisionIdFromNumberText(valueText);
                        if (window.useIScroll && window.rulesAndDecisionsListIScroll) {
                            window.rulesAndDecisionsListIScroll.scrollToElement("#" + decId, 0);
                        } else {//in case the iScroller is not present, we need to do this "manually"
                            document.getElementById("rules-and-decisions-list-wrapper").scrollTop = document.getElementById(decId).offsetTop;
                        }
                    },
                    onPosition: function (html, windowWidth) {
                        //add padding for the height of the spinning wheels
                        var dw = html.find(".dw"),
                            height = dw.innerHeight() - 1;//subtract one for the list-item's bottom-border
                        $("#rules-and-decisions-list").css("padding-bottom", height.toString() + "px");
                        if (window.useIScroll && window.rulesAndDecisionsListIScroll) {
                            window.rulesAndDecisionsListIScroll.refresh();
                        }
                        //add space for the rule selector on the right side
                        dw.width(windowWidth - $("#rule-selector").innerWidth());
                    }
                }).empty();//remove the source elements from DOM
                mbsInst = el.mobiscroll("getInst");
            },
            showExploreDecisions = _.debounce(function () {
                if (window.searchDisplayed) {
                    hideSearch(true);
                }
                window.exploreDecisionsDisplayed = true;
                if (mbsInst) {
                    mbsInst.show();
                }
            }, 300, true),
            hideExploreDecisions = _.debounce(function (reset) {
                if ($.mobiscroll.activeInstance) {
                    if (reset) {
                        window.exploreDecisionsOn = false;
                        if (mbsInst) {
                            mbsInst.clear();
                        }
                    }
                    if (mbsInst) {
                        mbsInst.hide();
                        $("#rules-and-decisions-list").css("padding-bottom", "0");
                        app.core.refreshIScroll(window.rulesAndDecisionsListIScroll, 10);
                    }
                    window.exploreDecisionsDisplayed = false;
                }
            }, 300, true),//prevent running twice on "close and move to another page"
        //search
            showSearch = _.debounce(function () {
                if (!window.useSearch) {
                    return;
                }
                if (window.searchDisplayed) {
                    if (!window.searchInput) {
                        _.delay(function () {
                            $("#search-input").focus();
                        }, 200);
                    }
                    return;
                }
                if (window.exploreDecisionsDisplayed) {
                    hideExploreDecisions(true);
                }
                window.searchDisplayed = true;
                if (app.searchView) {
                    sv.show();
                    if (window.searchInput) {
                        $("#search-input").val(window.searchInput);
                    } else {
                        app.searchView.clickClear();
                        _.delay(function () {
                            $("#search-input").focus();
                        }, 200);
                    }
                } else {
                    app.searchView = new app.SearchView();
                    app.searchView.render();
                    sv.html(app.searchView.$el).show();
                    _.delay(function () {
                        $("#search-input").focus();
                    }, 200);
                }
                rdlv.addClass("with-search");
                app.rulesAndDecisionsListView.onOrientationChange();
                _calculateHeight();
            }, 300, true),
            hideSearch = _.debounce(function (reset) {
                //if ($.isFunction(app.searchView.remove)) {
                //    app.searchView.remove();
                //}
                if (reset) {
                    window.searchOn = false;
                    app.searchView.clickClear();
                }
                sv.hide();//.empty();
                rdlv.removeClass("with-search");
                app.rulesAndDecisionsListView.onOrientationChange();
                _calculateHeight();
                window.searchDisplayed = false;
            }, 300, true),
            highlightFoundWords = function (txt) {
                if (typeof txt === "string" && txt.length > 0
                    && window.searchOn && window.searchInput && window.searchInputCleaned
                ) {
                    txt = txt.replace(
                        new RegExp("(" + window.searchInputCleaned.split(" ").join("|") + ")(?![^<]*>)", "img"),
                        "<span class='hlw'>$1</span>");
                }
                return txt;
            },
        //sharing
            extractTextFromHtml = function (str) {
                var tmp = document.implementation.createHTMLDocument("New").body;
                tmp.innerHTML = str;
                str = tmp.textContent || tmp.innerText || "";//extract text from HTML
                tmp = null;
                return str;
            },
            prepareMessageForSharing = function (str) {
                str = str.replace(/(?:\r\n|\n|\r)/g, "");//remove new lines
                str = str.replace(/<\/p><p/g, "</p>xXx<p");//preserve knowledge of paragraph breaks
                str = str.replace(/<br ?\/?>/g, "yYy");//preserve knowledge of <br>
                str = extractTextFromHtml(str);
                str = str.replace(/xXx/g, "\n\n");//re-create the paragraphs by simulation
                str = str.replace(/yYy/g, "\n");//re-create the <br> by simulation
                return str;
            },
            showSharing = function () {

                navigator.screenshot.URI(function (error, res) {
                    if (error) {
                        console.error(error);
                    } else {
                        console.log('ok', res.URI);
                        var subject = extractTextFromHtml(window.sharingSubject),
                            msg = "I consulted the R&A Decisions on the Rules of Golf app for a ruling on: " +
                                subject;
                        //"\n" +
                        //        "http://www.randa.org/RulesEquipment/Rules/Rules-Explorer";

                        //"\n\n" +
                        //window.sharingMessage +
                        //"\n\n" +
                        //"---" +
                        //"\n\n" +
                        //"You can find out more about this ruling at" +
                        //"\n" +
                        //"http://www.randa.org/RulesEquipment/Rules/Rules-Explorer" +
                        //"\n\n" +
                        //"---" +
                        //"\n\n" +
                        //"You can find out more about this app at ...";//TODO: need the link to the Google Play store
                        window.plugins.socialsharing.shareWithOptions({
                            message: msg,
                            subject: subject,
                            files: [res.URI],
                            url: 'http://www.randa.org/RulesEquipment/Rules/Rules-Explorer',
                        }, logger, logger);
                    }
                }, 50);
                //navigator.screenshot.save(function(error,res){
                //    if(error){
                //        console.error(error);
                //    }else{
                //        console.log('ok',res.filePath);
                //
                //
                //    }
                //});


            },
        //media
            attachMediaPopup = function (el) {
                if (typeof el === "string") {
                    el = $(el).first();
                }
                el.each(function (ignore, e) {
                    var $e = $(e);
                    if (!useHtml5VideoPlugin()) {
                        if ($e.hasClass("mfp-iframe")) {//pre-process the file pathadb uninstall
                            logger($e, "attachMediaPopup, $e");
                            $e.attr("data-mfp-src", "res/raw/" + cleanVideoFileName($e.attr("data-mfp-src")));
                        }
                    }
                    $e.magnificPopup({
                        callbacks: {
                            markupParse: function (template, values, item) {
                                values.title = item.el.attr("title");
                            },
                            open: function () {
                                this.contentContainer.closest(".mfp-container").hammer();
                                unlockOrientation();
                            },
                            change: function () {
                                var container = $(this.content),
                                    video = container.find(".mfp-iframe").first();
                                prepareVideoTags(container, video, true);
                            },
                            beforeClose: function () {
                                // Callback available since v0.9.0
                                console.log('Popup close has been initiated');
                                setPhoneViewIfNecessary();
                            },
                        },
                        delegate: ".mli",
                        /* http://dimsemenov.com/plugins/magnific-popup/documentation.html#iframe-type */
                        iframe: {
                            markup: useHtml5VideoPlugin()
                                //uses <div> to be replaced by <video>
                                ? '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><figure><div class="mfp-iframe html5Video" style="width:100%;"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>'
                                //uses <iframe>
                                : '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><figure><iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
                            patterns: {
                                local: {
                                    index: "video/",
                                    id: null,
                                    src: "%id%"
                                }
                            }
                        },
                        image: {
                            titleSrc: "title"
                        },
                        gallery: {
                            enabled: true,
                            preload: [1, 1],
                            navigateByImgClick: true
                        },
                        zoom: {
                            enabled: true,
                            duration: 300 // don't foget to change the duration also in CSS
                        }
                    });
                });
            },
            truncateMediaDesc = function () {
                var el = $(".mli-text");
                if (el.length > 0) {
                    if ($.isFunction($.ellipsis)) {
                        $.ellipsis(".mli-text", {maxLine: 2});
                    } else {
                        if ($.isFunction(el.dotdotdot)) {
                            el.dotdotdot({
                                wrap: "word",
                                height: 50,
                                tolerance: 5
                            });
                        }
                    }
                }
            },
        //video
            useHtml5VideoPlugin = function () {
                //return window.plugins["html5Video"] && !app.core.isAndroidKitKat4xAndAbove("9");
                var result = false;
                if (window.plugins["html5Video"]) {

                    switch (window["device"].version) {
                        case "4.4":
                        case "4.4.1":
                        case "4.4.2":
                            result = true;
                            break;
                        default:
                            result = false;
                            break;
                    }
                }
                return result;
            },
            cleanVideoFileName = function (fileName) {
                if (window.useDummyVideo) {
                    return "JDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9.mp4";
                }
                if (!window.cleanVideoFileNames) {
                    return fileName;
                }
                if (typeof fileName !== "string" || fileName.length === 0) {
                    return fileName || "";
                }
                //Android file system (raw) accepts only lower case letters and digits in the file names
                var fileNameArr = fileName.split("."),
                    fileExt = fileNameArr.pop();
                fileName = fileNameArr.join("");//get rid of further dots, if any
                fileName = fileName.toLowerCase().replace(/[^a-z0-9]/ig, "");//remove special chars
                if (/[0-9]/.test(fileName[0])) {
                    fileName = "a" + fileName;//prepend "a" for file name starting with a digit
                }
                return fileName + "." + fileExt;
            },
            videoLookupList = function () {
                var videoLookupTable = [
                    {
                        "OriginalName": "0_cbsasgsj_0_hk0xcnw6_2",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/a1",
                        "fName": "a1.mp4"
                    },
                    {
                        "OriginalName": "16-1",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/b1",
                        "fName": "b1.mp4"
                    },
                    {
                        "OriginalName": "BDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/c1",
                        "fName": "c1.mp4"
                    },
                    {
                        "OriginalName": "CDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/d1",
                        "fName": "d1.mp4"
                    },
                    {
                        "OriginalName": "DDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/e1",
                        "fName": "e1.mp4"
                    },
                    {
                        "OriginalName": "DOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/f1",
                        "fName": "f1.mp4"
                    },
                    {
                        "OriginalName": "F4MGo0Mzod7A01R2hIcSEJBPN-gIhyVn",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/g1",
                        "fName": "g1.mp4"
                    },
                    {
                        "OriginalName": "FDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/h1",
                        "fName": "h1.mp4"
                    },
                    {
                        "OriginalName": "IDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/i1",
                        "fName": "i1.mp4"
                    },
                    {
                        "OriginalName": "JDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/j1",
                        "fName": "j1.mp4"
                    },
                    {
                        "OriginalName": "QDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/k1",
                        "fName": "k1.mp4"
                    },
                    {
                        "OriginalName": "RDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/l1",
                        "fName": "l1.mp4"
                    },
                    {
                        "OriginalName": "SDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/m1",
                        "fName": "m1.mp4"
                    },
                    {
                        "OriginalName": "TDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/n1",
                        "fName": "n1.mp4"
                    },
                    {
                        "OriginalName": "UDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/o1",
                        "fName": "o1.mp4"
                    },
                    {
                        "OriginalName": "WDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/p1",
                        "fName": "p1.mp4"
                    },
                    {
                        "OriginalName": "YDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9",
                        "NewName": "android.resource://com.aimermedia.randa.golf2016/raw/q1",
                        "fName": "q1.mp4"
                    },
                ];
                return videoLookupTable;
            },
            prepareVideoTags = function (containerEl, videoEl, playFirst) {

                var videoSelectedId;

                if (typeof containerEl === "string") {
                    containerEl = $(containerEl).first();
                }
                if (typeof videoEl === "string") {
                    videoEl = containerEl.find(videoEl);
                }
                if (videoEl.length > 0) {//we deal with the videos
                    var videoTag,
                        videoSrc,
                        videoName,
                        videoNameArr,
                        videoFileName,
                        videoFileExt,
                        videoThumbName,
                        videoId,
                        videoClass,
                        videoStyle,
                        videoType,
                        newVideoTag,
                        videos;
                    videoEl.each(function (i, e) {
                        videoTag = $(e);
                        videoSrc = videoTag.attr("src");
                        if (!videoSrc) {
                            videoSrc = videoTag.data("file");
                            if (!videoSrc) {
                                videoSrc = videoTag.data("src");
                            }
                        }
                        if (videoSrc) {
                            videoName = videoSrc.split("/").pop();
                            videoNameArr = videoName.split(".");
                            videoFileExt = videoNameArr.pop().toLowerCase();
                            videoFileName = videoNameArr.join("");
                            videoThumbName = "img/videos/" + videoFileName + ".poster.jpg";
                            videoName = cleanVideoFileName(videoName);
                            videoId = "video" + (i + 1).toString();
                            videoSelectedId=videoFileName;
                            videoId = "video1";
                            videoClass = videoTag.attr("class");
                            videoStyle = videoTag.attr("style");
                            if (window.useVideoTags) {
                                // <video width="400px" height="300px"  controls>
                                //     <source src="android.resource://com.aimermedia.randa.golf2016/raw/i1" type="video/mp4">
                                // </video>
                                newVideoTag = $("<video  poster='" + videoThumbName + "' controls/>");

                                // newVideoTag = $("<video id='" + videoId + "' data-file='" + videoName + "' poster='" + videoThumbName + "'/>");
                                if (useHtml5VideoPlugin()) {
                                    newVideoTag = $("<video id='" + videoId + "' poster='" + videoThumbName + "' controls/>");
                                    // newVideoTag.addClass("html5Video").css("width", "100%");

                                    // newVideoTag.replaceWith($("<video/>", {
                                    //  id: "video1",
                                    //  "data-file": videoName
                                    //  }).addClass("html5Video").css("width", "100%"));

                                } else {
                                    var newSrc = videoFileName;
                                    var videoLookupTable = videoLookupList();
                                    for (i = 0; i < videoLookupTable.length; i++) {
                                        var anEntry = videoLookupTable[i];
                                        if (anEntry['OriginalName'] == videoFileName) {
                                            newSrc = anEntry['NewName'];
                                            break;
                                        }

                                    }

                                    $("<source>", {
                                        src: newSrc,
                                        // src: "video/" + videoName,
                                        type: "video/mp4"
                                    }).appendTo(newVideoTag);
                                }
                                newVideoTag.addClass(videoClass).attr({//re-apply the attributes
                                    style: videoStyle,
                                    webkitPlaysinline: "webkit-playsinline"
                                }).css({
                                    maxWidth: "100%",
                                    height: "auto",
                                    zIndex: 3
                                });
                                videoTag.replaceWith(newVideoTag);
                                // unlockOrientation();
                            }
                        }
                    });
                    // videos = findAndInitVideos(containerEl);
                    videos = findAndInitVideos(videoSelectedId);
                    if (playFirst && videos.length > 0) {
                        _.delay(function () {
                            playVideo(videos[0]);
                        }, 300);
                    }
                    return videos;
                }
                return [];
            },
        // findAndInitVideos = function (el) {
            findAndInitVideos = function (videoFileName) {
                if (!useHtml5VideoPlugin()) {
                    return [];
                }

              var mp4;
                var videoLookupTable = videoLookupList();
                for (var i = 0; i < videoLookupTable.length; i++) {
                    var anEntry = videoLookupTable[i];
                    if (anEntry['OriginalName'] == videoFileName) {
                        mp4=anEntry['fName'];
                        break;
                    }
                }
                var tempVid={};
                tempVid['video1']= mp4;
                window.plugins.html5Video.initialize(tempVid);
                return ['video1'];

                // window.plugins.html5Video.initialize({
                //     "0_cbsasgsj_0_hk0xcnw6_2" : "a1.mp4",
                //     "16-1" : "b1.mp4",
                //     "BDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9" : "c1.mp4",
                //     "FDOcJ-FxaFrRg4gtGEwOnI5OjBoO3pKt9" : "h1.mp4",
                // })
                return [videoId];


                // var out = [];
                // var newVideoId;
                // var initObj = {};
                // var videoLookupTable = videoLookupList();
                // for (var i = 0; i < videoLookupTable.length; i++) {
                //     var anEntry = videoLookupTable[i];
                //     if (anEntry['OriginalName'] == videoId) {
                //         var newSrc = anEntry['fName'];
                //         initObj[videoId] = newSrc;
                //         var vidRef={videoId,newSrc};
                //         window.plugins["html5Video"].initialize(vidRef);
                //         out.push(videoId);
                //         break;
                //     }
                // }
                // if (out.length > 0) {
                //     window.plugins["html5Video"].initialize(initObj);
                // }
                return out;
                // if (typeof el === "string") {
                //     el = $(el).first();
                // }
                // var videoTag = el.find(".html5Video"),
                //     videoId,
                //     videoFile,
                //     initObj = {},
                //     out = [];
                // if (videoTag.length > 0) {
                //     videoTag.each(function (ignore, e) {
                //         var $e = $(e);
                //         videoId = $e.attr("id");
                //         videoFile = $e.data("file");
                //         videoId = "video1";
                //         videoFile = "q1.mp4"
                //         if (videoId && videoFile) {
                //             initObj[videoId] = videoFile;
                //             out.push(videoId);
                //         }
                //     });
                //     if (out.length > 0) {
                //         window.plugins["html5Video"].initialize(initObj);
                //     }
                // }
                // return out;//return the array of video tags' IDs
            },
            playVideo = function (id, callback) {
                var videoTag;
                if (useHtml5VideoPlugin()) {
                    window.plugins["html5Video"].play(id, callback);
                    videoTag = $("#" + id);
                } else {
                    videoTag = document.getElementById(id);
                    videoTag.addEventListener('canplay', function () {
                        videoTag.play();
                    });
                    videoTag.load();
                    videoTag.play();
                }
                $.each(videoTag, function (i, e) {
                    logger(e, "playVideo, videoTag, " + i);
                });
            },
            scaleVideoIFrame = function () {
                var el = _currentView.$el.find(".iframe-scaler");
                //logger(_currentView.$el, "scaleVideoIFrame, view $el");
                if (el.length > 0) {
                    el.each(function (i, e) {
                        var $e = $(e);
                        $e.height($e.width());
                        _.delay(function () {
                            var iframe = $e.find("iframe")[0];
                            var video = $(iframe.contentWindow.document).find("video").first();
                            //logger(video, "scaleVideoIFrame, video");
                            if (video.length > 0) {
                                var videoWidth = video.width(),
                                    videoHeight = video.height();
                                //logger(videoWidth, "scaleVideoIFrame, video width");
                                //logger(videoHeight, "scaleVideoIFrame, video height");
                                if (videoWidth && videoHeight) {
                                    $e.width(videoWidth);//give the video iframe proper proportions
                                    $e.height(videoHeight);
                                }
                            }
                        }, 500);//wait for the video loading and resizing
                    });
                }
            },
        //handlers
            h = {
                navigateToRule: function (e) {
                    var data = $(e.target).data("rule");
                    data = removeLettersAfterTheSubRuleDigits(data).replace("–", "-");//replace incorrect en dash for hyphen
                    if (data) {
                        var hash = "#rule/" + encodeURIComponent(data);
                        if (window.location.hash !== hash) {
                            _.delay(function () {
                                //spinnerDialogShow();
                                window.location.hash = hash;
                            }, 30);
                        }
                    }
                    return false;
                },
                navigateToAppendix: function (e) {
                    var data = $(e.target).data("appendix");
                    //logger(data, "navigateToAppendix, appendix", true);
                    if (data) {
                        var temp,
                            id;
                        temp = trim(data.toLowerCase().replace("appendix", ""));
                        switch (temp) {
                            case "i":
                            case "ia":
                                id = 4;
                                break;
                            case "ib":
                                id = 3;
                                break;
                            case "ii":
                                id = 1;
                                break;
                            case "iii":
                                id = 2;
                                break;
                            case "iv":
                                id = 5;
                                break;
                        }
                        id = id || data;
                        var hash = "#appendix/" + encodeURIComponent(id);
                        if (window.location.hash !== hash) {
                            _.delay(function () {
                                //spinnerDialogShow();
                                window.location.hash = hash;
                            }, 30);
                        }
                    }
                    return false;
                },
                navigateToDecision: function (e) {
                    var data = $(e.target).data("decision");
                    if (data) {
                        var hash = "#decision/" + encodeURIComponent(data);
                        if (window.location.hash !== hash) {
                            _.delay(function () {
                                //spinnerDialogShow();
                                window.location.hash = hash;
                            }, 30);
                        }
                    }
                    return false;
                },
                highlightLinkOrDefinition: function (e) {
                    $("a").removeClass("hl");
                    $("span.definition").removeClass("hl");
                    $(e.target).addClass("hl");
                },
                showDefinition: function (e) {
                    h.highlightLinkOrDefinition(e);
                    var view = new app.DefinitionView();
                    view.definition = $(e.target).text().replace(/[\.,;]$/g, "");//remove oddities from the end of the string
                    dfv.html(view.render().$el);
                    return false;
                },
                onBackButton: function () {
                    logger("onBackButton onBackButtononBackButtononBackButtononBackButtononBackButton");
                    if ($.magnificPopup.instance.isOpen) {
                        var result = $.magnificPopup.close();
                        logger("onBackButton onBackButtononBackButtononBackButtononBackButtononBackButton[" + result + "]");
                        return;
                    }
                    var useDelay = false;
                    if (window.menuStatus === "opened") {
                        hideMenu();
                        useDelay = true;
                    }
                    if (window.addToFolderStatus === "opened") {
                        hideAddToFolder();
                        useDelay = true;
                    }
                    _.delay(function () {
                        goBackInHistoryStack(null, true);
                    }, useDelay ? 250 : 30);
                    return false;
                },
                zoomIn: function () {
                    logger("zoomIn");
                },
                zoomOut: function () {
                    logger("zoomOut");
                }
            },
        //app starting
            attachHandlers = function () {
                /*
                 $("body").on("click mousedown mouseup focus blur keydown change mouseup click dblclick mousemove mouseover mouseout mousewheel keydown keyup keypress textInput touchstart touchmove touchend touchcancel resize scroll zoom focus blur select change submit reset", function (e) {
                 app.core.logEvent(e);
                 });
                 //*/
                $(document)
                    .on("click", "a.link-to-rule", h.navigateToRule)
                    .on("click", "a.link-to-appendix", h.navigateToAppendix)
                    .on("click", "a.link-to-decision", h.navigateToDecision)
                    .on("click", "a", h.highlightLinkOrDefinition)
                    .on("click", "span.definition", h.showDefinition)
                    .on("click", ".mfp-content a", $.magnificPopup.close)//close popup upon hitting link within the caption
                    .on("click", ".mfp-title", $.magnificPopup.close)//close popup upon clicking the caption extending from below the image padding
                    .on("swipeleft swipeLeft", ".mfp-container", $.magnificPopup.instance.next)
                    .on("swiperight swipeRight", ".mfp-container", $.magnificPopup.instance.prev)
                    .on("backbutton", h.onBackButton)
                    .on("menubutton", showMenu)
                    .on("click swipeleft swipeLeft", "#menu-view-close", hideMenu)
                    .on("swipeleft swipeLeft", "#menu-view-i", hideMenu)
                    .on("click swiperight swipeRight", "#add-to-folder-view-close", hideAddToFolder)
                    .on("pinchIn", h.zoomIn)
                    .on("pinchOut", h.zoomOut);
                $(window).on("orientationchange resize", function () {
                    if (screen.orientation && screen.orientation.type && screen.orientation.type.substr(0, 9) === "landscape") {
                        $("body").addClass("landscape");
                    } else {
                        $("body").removeClass("landscape");
                    }
                    if (_currentView && typeof _currentView.onOrientationChange === "function") {
                        _currentView.onOrientationChange.apply(_currentView);
                    }
                    _.delay(function () {
                        hv.html((new app.HeaderView()).render().$el);//updates header icons, etc.
                    }, 100);
                    app.core.refreshLogIScroll();
                    //app.core.refreshContentIScroll();
                });
            },
            setPhoneViewIfNecessary = function () {
                var w = $(window),
                    ww = w.width(),
                    wh = w.height();
                if (ww < 525 || wh < 525) {//landscape 600 minus header and footer equals 529
                    $("body").addClass("phone-view");
                    window.isPhoneView = true;
                    lockOrientation();
                } else {
                    window.isPhoneView = false;
                }
                logger("window, width: " + ww + "px, height: " + wh + "px");
                logger(window.isPhoneView, "window.isPhoneView");
            },
            setDefaultOptions = function () {
                if (window.useNativeAnimation) {
                    //native page transitions
                    window.plugins["nativepagetransitions"]["globalOptions"].duration = 333;
                    window.plugins["nativepagetransitions"]["globalOptions"].iosdelay = -1;
                    window.plugins["nativepagetransitions"]["globalOptions"].androiddelay = -1;
                    window.plugins["nativepagetransitions"]["globalOptions"].winphonedelay = 175;
                    window.plugins["nativepagetransitions"]["globalOptions"].slowdownfactor = 8;
                    //these are used for slide left/right only currently
                    window.plugins["nativepagetransitions"]["globalOptions"].fixedPixelsTop = 44;
                    window.plugins["nativepagetransitions"]["globalOptions"].fixedPixelsBottom = 0;
                }
            },
            preCacheData = function () {
                getAndCacheTheRuleSequence();
                getAndCacheTheDecisionSequence(createExploreDecisions);
                //getAndCacheTheSearchSequence();
            },
            startApp = function () {
                app.router = new Router();
                Backbone.history.start();
                setPhoneViewIfNecessary();
                $(window).trigger("orientationchange");
            },
            onDeviceReady = function () {
                if (Backbone === undefined) {
                    alert('Backbone framework is missing');
                }
                if (!app.core) {
                    alert('core module is missing');
                }
                app.core.init();
                if (window["device"] && window["device"].platform === "Android") {
                    switch (window["device"].version) {
                        case "4.4":
                        case "4.4.1":
                        case "4.4.2":
                            break;
                        default:
                            FastClick.attach(document.body);
                            break;
                    }
                } else {
                    FastClick.attach(document.body);
                }

                //lockOrientation();
                //spinnerDialogShow();
                openDb();
                app.core.initLog();
                app.core.initAjax();
                window.plugins.insomnia.keepAwake();
                initViewHolders();
                attachHandlers();
                setDefaultOptions();
                _.delay(preCacheData, 800);
                _.delay(startApp, 1500);
            };
        return {
            replaceRouteInHistoryStack: replaceRouteInHistoryStack,
            getKindFromHistoryStack: getKindFromHistoryStack,
            goBackInHistoryStack: goBackInHistoryStack,
            removeLettersAfterTheSubRuleDigits: removeLettersAfterTheSubRuleDigits,
            makeDecisionIdFromNumberText: makeDecisionIdFromNumberText,
            makeSortableDecisionId: makeSortableDecisionId,
            convertAppendixImages: convertAppendixImages,
            convertDecisionImage: convertDecisionImage,
            convertLinks: convertLinks,
            convertMediaLinks: convertMediaLinks,
            highlightListItemByAddition: highlightListItemByAddition,
            highlightListItemByRetrieval: highlightListItemByRetrieval,
            replaceImgBySvg: replaceImgBySvg,
            setTemporaryOptionsForLateralMovements: setTemporaryOptionsForLateralMovements,
            toggleHolder: toggleHolder,
            highlightClickedButton: highlightClickedButton,
            showMenu: showMenu,
            hideMenu: hideMenu,
            showAddToFolder: showAddToFolder,
            hideAddToFolder: hideAddToFolder,
            showExploreDecisions: showExploreDecisions,
            hideExploreDecisions: hideExploreDecisions,
            showSearch: showSearch,
            hideSearch: hideSearch,
            prepareMessageForSharing: prepareMessageForSharing,
            showSharing: showSharing,
            highlightFoundWords: highlightFoundWords,
            attachMediaPopup: attachMediaPopup,
            truncateMediaDesc: truncateMediaDesc,
            cleanVideoFileName: cleanVideoFileName,
            prepareVideoTags: prepareVideoTags,
            findAndInitVideos: findAndInitVideos,
            playVideo: playVideo,
            scaleVideoIFrame: scaleVideoIFrame,
            onDeviceReady: onDeviceReady
        }
    }();
}(jQuery || Zepto));
//some custom improvements
_.extend(Backbone.Router.prototype, {
    navigateCustom: function (fragment, options) {
        if (options.replace === true) {//handle lateral movement
            app.main.replaceRouteInHistoryStack(fragment.replace(/^#/, ""));
            window.lateralMovementDetected = true;
        }
        Backbone.Router.prototype.navigate.apply(this, arguments);
    }
});
//main trigger
window.onload = function () {
    document.addEventListener("deviceready", app.main.onDeviceReady, false);
};