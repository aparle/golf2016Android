window.numberOfCPUs = 0;
window.useLogger = true;
window.useIScroll = true;
window.useIScrollSelective = true;
window.useAnimation = false;
window.useNativeAnimation = false;
window.useNativeMenuAnimation = false;
window.app = {};
window.templateCache = {};
app.startTime = new Date();
if (typeof jQuery === "undefined") {
    jQuery = 0;
}
(function ($) {
    "use strict";
    //core module
    app.core = function () {
        var $log,
            $logW,
            $toggleLog,
            logIScroll,
            contentIScroll,
            isAndroidKitKat44AndAboveRes = [],
            iScrollClickRes,
            visibleLogging = false,//make the logging visible (or not)
            usingEConsole = (typeof window["econsole"] !== "undefined"),
        //handlers
            h = {
                toggleLog: function () {
                    $logW.toggle();
                    var logWVisible = $logW.filter(":visible");
                    if (logWVisible && logWVisible.length > 0) {
                        refreshLogIScroll();
                        $toggleLog.show();
                    } else {
                        $toggleLog.hide();
                    }
                    return false;
                }
            },
            /**
             * @see http://stackoverflow.com/a/16187766
             */
            cmpVersions = function (a, b) {
                var i, l, diff, segmentsA, segmentsB;
                segmentsA = a.replace(/(\.0+)+$/, "").split(".");
                segmentsB = b.replace(/(\.0+)+$/, "").split(".");
                l = Math.min(segmentsA.length, segmentsB.length);
                for (i = 0; i < l; i++) {
                    diff = parseInt(segmentsA[i], 10) - parseInt(segmentsB[i], 10);
                    if (diff !== 0) {
                        return diff;
                    }
                }
                return segmentsA.length - segmentsB.length;
            },
            isAndroidKitKat4xAndAbove = function (subVersion) {
                if (isAndroidKitKat44AndAboveRes[subVersion] !== undefined) {
                    return isAndroidKitKat44AndAboveRes[subVersion];
                }
                var out = false;
                if (window.device.platform === "Android") {
                    if (subVersion === undefined || subVersion === null) {
                        subVersion = "4";
                    } else {
                        subVersion = subVersion.toString();
                    }
                    out = cmpVersions(window.device.version, "4." + subVersion);
                    logger(out, "isAndroidKitKat4xAndAbove, subVersion: " + subVersion + ", out");
                    out = (out >= 0);
                }
                isAndroidKitKat44AndAboveRes[subVersion] = out;
                return out;
            },
            initLog = function () {
                if (visibleLogging) {
                    $log = $("#log");
                    $logW = $("#logW");
                    $toggleLog = $("#toggle-log");
                    $log.css("visibility", "visible");
                    $toggleLog.show().click(h.toggleLog);
                    if (typeof shake !== "undefined") {
                        shake.startWatch(h.toggleLog);
                        $toggleLog.text("Click here or shake the device to hide the log");
                    }
                    //h.toggleLog();//initially hide the log
                    logIScroll = createIScroll("#logW");
                }
            },
            addLog = function (str, noTime, isImportant) {
                if (!noTime) {
                    var d = new Date();
                    str = ((d.getTime() - app.startTime.getTime()) / 1000).toString() + " - " + str;
                }
                var para = $("<p/>").text(str);
                if (isImportant) {
                    para.css({
                        fontSize: "larger",
                        fontWeight: "bold",
                        color: "#c03"
                    });
                }
                $("#log").prepend(para);
            },
            logger = function (val, name, important) {
                if (!window.useLogger) {
                    return;
                }
                if (usingEConsole) {
                    window["econsole"].log(val);
                } else {
                    if (typeof val === "object") {
                        if (visibleLogging) {
                            if (name) {
                                addLog(name + ":");
                            }
                            $.each(val, function (i, e2) {
                                if (val.hasOwnProperty(i) || parseInt(i) || i === 0) {//can be also index of an array
                                    if ($.isPlainObject(e2)) {
                                        addLog(i + ": " + JSON.stringify(e2), true);
                                    } else if (typeof e2 === "object") {
                                        addLog(i + ": [object]", true);
                                    } else {
                                        addLog(i + ": " + e2, true);
                                    }
                                }
                            });
                            refreshLogIScroll();
                        }
                        if (name) {
                            console.log(name + ":");
                        }
                        console.dir(val);
                        $.each(val, function (i, e) {
                            if (val.hasOwnProperty(i)) {
                                if (typeof e !== "object") {
                                    console.log(i + ": " + e);
                                }
                            }
                        });
                    } else {
                        if (visibleLogging) {
                            addLog((name ? name + ": " : "") + val, null, important);
                            refreshLogIScroll();
                        }
                        console.log((name ? name + ": " : "") + val);
                    }
                }
            },
            logEvent = function (e) {
                logger(e.type, e.target.tagName + (e.target.id ? "." + e.target.id : "") + ", event");
            },
            initAjax = function () {
                $.support.cors = true;
                //$.mobile.allowCrossDomainPages = true;
            },
            iterifyArr = function (arr) {
                if ($.isArray(arr)) {
                    var cur = 0;
                    arr.getCur = function () {
                        return cur;
                    };
                    arr.next = function () {
                        return (++cur >= this.length)
                            ? false
                            : this[cur];
                    };
                    arr.prev = function () {
                        return (--cur < 0)
                            ? false
                            : this[cur];
                    };
                    arr.reset = function () {
                        cur = 0;
                    };
                    arr.setCur = function (num) {
                        cur = num;
                    };
                }
                return arr;
            },
            iScrollClick = function () {
                if (iScrollClickRes !== undefined) {
                    return iScrollClickRes;
                }
                var out = true;
                // workaround click bug iscroll #674
                if (/iPhone|iPad|iPod|Macintosh/i.test(navigator.userAgent)) {
                    out = false;
                }
                if (/Chrome/i.test(navigator.userAgent)) {
                    out = (/Android/i.test(navigator.userAgent));
                }
                if (/Silk/i.test(navigator.userAgent)) {
                    out = false;
                }
                if (/Android/i.test(navigator.userAgent)) {
                    out = isAndroidKitKat4xAndAbove();
                }
                iScrollClickRes = out;
                return out;
            },
            createIScroll = function (el, noScrollBars, horizontal, forceNative) {
                var $el,
                    out;
                if (typeof el === "string") {
                    $el = $(el).first();
                } else {
                    $el = el;
                }
                if (window.useIScroll && !forceNative) {
                    var opt = {
                        scrollX: !!horizontal,
                        scrollY: !horizontal,
                        click: iScrollClick()
                    };
                    if (!noScrollBars) {
                        opt.scrollbars = true;
                        opt.fadeScrollbars = true;
                        opt.shrinkScrollbars = "clip";
                    }
                    out = new IScroll(el, opt);
                } else {
                    if (horizontal) {
                        $el.addClass("webkit-scroll-horizontal");
                    } else {
                        $el.addClass("webkit-scroll-vertical");
                    }
                    out = null;
                }
                return out;
            },
            refreshIScroll = function (iScrollInstance, time, callback) {
                if (!window.useIScroll) {
                    return;
                }
                _.delay(function () {
                    if (iScrollInstance && iScrollInstance.options) {
                        iScrollInstance.refresh();
                        _.delay(function () {
                            if (iScrollInstance && iScrollInstance.options) {//check again
                                iScrollInstance.resetPosition();
                            }
                            if (typeof callback === "function") {
                                callback();
                            }
                        }, 50);
                    }
                }, time || 200);
            },
            refreshLogIScroll = function () {
                refreshIScroll(logIScroll, 200, function () {
                    /*
                     var para = $log.find("p");
                     if (para.length > 0) {
                     logIScroll.scrollToElement(para.last());
                     }
                     //*/
                });
            },
            refreshContentIScroll = function () {
                refreshIScroll(contentIScroll);
            },
        //cpu
            getCPUInfo = function (callback) {
                var system = navigator.system;
                if (!system && typeof xwalk !== "undefined") {
                    system = xwalk.experimental.system;
                    if (system) {
                        system.getCPUInfo().then(
                            function (info) {
                                logger(info, "getCPUInfo");
                                window.numberOfCPUs = info.numOfProcessors || 1;
                                if ($.isFunction(callback)) {
                                    callback();
                                }
                            },
                            function (error) {
                                logger(error, "getCPUInfo, error");
                                if ($.isFunction(callback)) {
                                    callback();
                                }
                            });
                    }
                } else {
                    if ($.isFunction(callback)) {
                        callback();
                    }
                }
            },
        //init
            init = function () {
                logger(navigator.userAgent, "navigator.userAgent");
                logger(window["device"], "window.device");
                logger(cordova.file, "cordova.file");
                getCPUInfo(function () {
                    logger(window.plugins["nativepagetransitions"], "native page transitions plugin");
                    window.useNativeAnimation = (
                        (isAndroidKitKat4xAndAbove("4") && window.numberOfCPUs >= 4)
                        || isAndroidKitKat4xAndAbove("9")
                    ) ? !!window.plugins["nativepagetransitions"]
                        : false;
                    window.useIScroll = window.useIScroll &&
                        (window.useIScrollSelective || !!(isAndroidKitKat4xAndAbove("9") || window.numberOfCPUs >= 4));
                });
            };
        return {
            initLog: initLog,
            logger: logger,
            logEvent: logEvent,
            initAjax: initAjax,
            iterifyArr: iterifyArr,
            isAndroidKitKat4xAndAbove: isAndroidKitKat4xAndAbove,
            createIScroll: createIScroll,
            refreshIScroll: refreshIScroll,
            refreshLogIScroll: refreshLogIScroll,
            refreshContentIScroll: refreshContentIScroll,
            init: init
        }
    }();
}(jQuery || Zepto));
function logger(val, name, important) {
    app.core.logger(val, name, important);
}
//some custom improvements
_.extend(Backbone.View.prototype, {
    templateFromFile: function (templatePath, el, data, callback) {
        if (!templatePath) {
            return;//no file path supplied
        }
        if (typeof el === "string") {
            el = $("#" + el);
        }
        if (!el || el.length === 0) {
            return;//nowhere to attach
        }
        var processFoundTemplate = function (template) {
            el.html(_.template(template, {variable: "vars"})(data || {}));
            if (typeof callback === "function") {
                callback();
            }
        };
        if (window.templateCache[templatePath]) {
            //retrieve the template from the cache, then process it
            logger("template retrieved from cache: " + templatePath);
            processFoundTemplate(window.templateCache[templatePath]);
        } else {
            //get and cache the template from the file
            $.get(templatePath, function (template) {
                logger("template file found: " + templatePath);
                window.templateCache[templatePath] = template;
                processFoundTemplate(template);
            });
        }
    },
    assign: function (view, selector) {
        view.setElement(this.$(selector)).render();
    }
});