// (c) 2014 Shuhei Funato
// http://blog.fspm.jp/2014/06/androidpinch-inoutjsgesturestart.html
// (c) 2016 modified by Tomas J Stehlik
var funcGestureStart = function (e) {
};
var funcGestureChange = function (e) {
};
var funcGestureEnd = function (e) {
};
// iOS向けイベント設定
document.body.addEventListener('gesturestart', funcGestureStart, false);
document.body.addEventListener('gesturechange', funcGestureChange, false);
document.body.addEventListener('gestureend', funcGestureChange, false);
// Android向けイベント設定
var pinchDistance = 0;
document.body.addEventListener('touchstart', function (e) {
    if (e.touches.length > 1) {
        pinchDistance = Math.sqrt(Math.pow((e.touches[1].pageX - e.touches[0].pageX), 2) + Math.pow((e.touches[1].pageY - e.touches[0].pageY), 2));
        funcGestureStart(e);
    } else {
        pinchDistance = 0;
    }
}, false);
document.body.addEventListener('touchmove', function (e) {
    if (pinchDistance <= 0) {
        return;
    }
    var newDistance = Math.sqrt(Math.pow((e.touches[1].pageX - e.touches[0].pageX), 2) + Math.pow((e.touches[1].pageY - e.touches[0].pageY), 2));
    var event = {scale: newDistance / pinchDistance};
    funcGestureChange(event);
});
document.body.addEventListener('touchmove', function (e) {
    if (pinchDistance <= 0) {
        return;
    }
    funcGestureEnd(e);
});
//     Zepto.js
//     (c) 2010-2016 Thomas Fuchs
//     (c) 2016 modified by Tomas J Stehlik
//     Zepto.js may be freely distributed under the MIT license.
(function ($) {
    var gesture = {}, gestureTimeout,
        parentIfText = function (node) {
            return 'tagName' in node ? node : node.parentNode
        };
    $(document).bind('gesturestart', function (e) {
        var now = Date.now(), delta = now - (gesture.last || now);
        gesture.target = parentIfText(e.target);
        gestureTimeout && clearTimeout(gestureTimeout);
        gesture.e1 = e.scale;
        gesture.last = now;
    }).bind('gesturechange', function (e) {
        gesture.e2 = e.scale;
    }).bind('gestureend', function () {
        if (gesture.e2 > 0) {
            Math.abs(gesture.e1 - gesture.e2) != 0 && $(gesture.target).trigger('pinch') &&
            $(gesture.target).trigger('pinch' + (gesture.e1 - gesture.e2 > 0 ? 'In' : 'Out'));
            gesture.e1 = gesture.e2 = gesture.last = 0;
        } else if ('last' in gesture) {
            gesture = {};
        }
    });
    ['pinch', 'pinchIn', 'pinchOut'].forEach(function (m) {
        $.fn[m] = function (callback) {
            return this.bind(m, callback);
        }
    })
})(jQuery || Zepto);