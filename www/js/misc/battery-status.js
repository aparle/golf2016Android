var batteryLevel, batteryIsPlugged, batteryLow, batteryCritical;
function initBatteryStatus() {
    window.addEventListener("batterystatus", function (info) {
        batteryLevel = info.level;
        batteryIsPlugged = info.isPlugged;
    }, false);
    window.addEventListener("batterylow", function () {
        batteryLow = true;
    }, false);
    window.addEventListener("batterycritical", function () {
        batteryCritical = true;
    }, false);
}
function checkBatteryLow(warn) {
    var out = false;
    if (batteryLow || batteryLevel < 15) {
        out = true;
        if (warn) {
            navigator.notification.alert("Battery level low, put it on charge now");
        }
    }
    return out;
}
function checkBatteryCritical(warn) {
    var out = false;
    if (batteryCritical || batteryLevel < 10) {
        out = true;
        if (warn) {
            navigator.notification.alert("Battery level critical, can't proceed");
        }
    }
    return out;
}