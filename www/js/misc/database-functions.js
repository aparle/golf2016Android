var DB;
function openDb() {
    logger("open database");
    if (isEmpty(DB)) {
        DB = window.sqlitePlugin.openDatabase({
            name: "golf2016.db",
            location: 2,
            createFromLocation: 1,
            androidLockWorkaround: 1
        });
    }
    setTimeout(function () {
        if (isEmpty(DB)) {
            openDb();
        }
    }, 200);
}
function queryFailed(tx, err) {
    logger(JSON.stringify({"tx": tx, "err": err}), "QUERY FAILED", true);
}
function processSql(commands) {
    Sql(DB, commands);
}
function retrieveData(commands) {
    processSql(commands);
}
function Sql(db, sqlList) {
    db.transaction(function (transaction) {
        for (var i = 0; i < sqlList.length; i++) {
            (function (tx, sql) {
                if (typeof(sql) === 'string') {
                    sql = [sql];
                }
                if (typeof(sql[1]) === 'string') {
                    sql[1] = [sql[1]];
                }
                var args = (typeof(sql[1]) === 'object')
                    ? sql.splice(1, 1)[0]
                    : [];
                var sql_return = sql[1] || function () {
                    };
                var sql_error = sql[2] || _sqlError(sql[0]);
                tx.executeSql(sql[0], args, sql_return, sql_error);
            }(transaction, sqlList[i]));
        }
    });
}
function queryAndProcessSqlResult(query, outputObject, key, callback) {
    var cb = function (out) {
        if (typeof callback === "function") {
            callback(out);
        }
    };
    if (outputObject === undefined || _.isEmpty(outputObject)) {
        outputObject = key
            ? {}
            : [];
        retrieveData([[
            query,
            function (tx, res) {
                var i,
                    k,
                    l = res.rows.length;
                if (l) {
                    for (i = 0; i < l; i++) {
                        k = key
                            ? res.rows.item(i)[key]
                            : i;
                        outputObject[k] = res.rows.item(i);
                    }
                }
                cb(outputObject);
            },
            function (tx, res) {
                cb(outputObject);
                queryFailed(tx, res);
            }
        ]]);
    } else {
        cb(outputObject);
    }
}
var _sqlError = function (lastSQL) {
    return function (tx, err) {
        logger("SQLITE ERROR: {errmsg} (Code {errcode}) {sql}".supplant({
            "errmsg": err.message,
            "errcode": err.code,
            "sql": lastSQL
        }), null, true);
        return false;
    };
};
