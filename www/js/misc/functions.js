var spinnerDialogShown;
function spinnerDialogShow() {
    if (spinnerDialogShown) {
        return;
    }
    spinnerDialogShown = true;
    window.plugins.spinnerDialog.show(null, "Please wait...", true);
}
function spinnerDialogHide() {
    spinnerDialogShown = false;
    window.plugins.spinnerDialog.hide();
}
window.onerror = function (message, url, lineNumber) {
    logger([message, url, lineNumber]);
    return true;
};
function isEmpty(theValue) {
    return (!theValue
    || (typeof(theValue) === "object" && $.isEmptyObject(theValue))
    || ($.isArray(theValue) && !theValue.length));
}
if (typeof trim !== "function") {
    function trim(str) {
        if (typeof str !== "string" || str.length === 0) {
            return str;
        }
        return str.replace(/^\s+|\s+$/gm, "");
    }
}
function trimLower(str) {
    if (typeof str !== "string" || str.length === 0) {
        return str;
    }
    return trim(str).toLowerCase();
}
if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g, function (a, b) {
            var r = o[b];
            return typeof r === 'string' || typeof r === 'number' ? r : a;
        });
    };
}
Array.prototype.diff = function (a) {
    return this.filter(function (i) {
        return a.indexOf(i) < 0;
    });
};
function lockOrientation() {
    screen.lockOrientation("portrait-primary");
    if (screen.orientation && screen.orientation.type
        && (screen.orientation.type.substr(0, 8) === "landscape" || screen.orientation.type === "portrait-secondary")
    ) {
        navigator.notification.alert("Rotate your device");
        navigator.vibrate(3000);
    }
}
function unlockOrientation() {
    screen.unlockOrientation();
}
function setPhoneViewIfNecessary() {
    var w = $(window),
        ww = w.width(),
        wh = w.height();
    if (ww < 525 || wh < 525) {//landscape 600 minus header and footer equals 529
        $("body").addClass("phone-view");
        window.isPhoneView = true;
        lockOrientation();
    } else {
        window.isPhoneView = false;
    }
    logger("window, width: " + ww + "px, height: " + wh + "px");
    logger(window.isPhoneView, "window.isPhoneView");
}
function makeCRCTable() {
    var c,
        crcTable = [];
    for (var n = 0; n < 256; n++) {
        c = n;
        for (var k = 0; k < 8; k++) {
            c = ((c & 1) ? (0xEDB88320 ^ (c >>> 1)) : (c >>> 1));
        }
        crcTable[n] = c;
    }
    return crcTable;
}
function crc32(str) {
    var crcTable = window.crcTable || (window.crcTable = makeCRCTable()),
        crc = 0 ^ (-1);
    for (var i = 0; i < str.length; i++) {
        crc = (crc >>> 8) ^ crcTable[(crc ^ str.charCodeAt(i)) & 0xFF];
    }
    return (crc ^ (-1)) >>> 0;
}
function getElementFromUnderTouch(e) {
    //logger(e, "getElementFromUnderTouch, event");
    var event = e.originalEvent || e,
        myLocation = event.touches[0] || event.changedTouches[0] || event.targetTouches[0];
    return document.elementFromPoint(myLocation.clientX, myLocation.clientY)
}
/**
 * jquery.binarytransport.js
 *
 * @description. jQuery ajax transport for making binary data type requests.
 * @version 1.0
 * @author Henry Algus <henryalgus@gmail.com>
 */
function usingZepto() {
    return (typeof Zepto !== "undefined");
}
// use this transport for "binary" data type
(function ($) {
    if (typeof $.ajaxTransport === "function") {
        $.ajaxTransport("+binary", function (options, originalOptions, jqXHR) {
            // check for conditions and support for blob / arraybuffer response type
            if (window.FormData && ((options.dataType && (options.dataType == 'binary')) || (options.data && ((window.ArrayBuffer && options.data instanceof ArrayBuffer) || (window.Blob && options.data instanceof Blob))))) {
                return {
                    // create new XMLHttpRequest
                    send: function (headers, callback) {
                        // setup all variables
                        var xhr = new XMLHttpRequest(),
                            url = options.url,
                            type = options.type,
                            async = options.async || true,
                        // blob or arraybuffer. Default is blob
                            dataType = options.responseType || "blob",
                            data = options.data || null,
                            username = options.username || null,
                            password = options.password || null;
                        xhr.addEventListener('load', function () {
                            var data = {};
                            data[options.dataType] = xhr.response;
                            // make callback and send data
                            callback(xhr.status, xhr.statusText, data, xhr.getAllResponseHeaders());
                        });
                        xhr.open(type, url, async, username, password);
                        // setup custom headers
                        for (var i in headers) {
                            xhr.setRequestHeader(i, headers[i]);
                        }
                        xhr.responseType = dataType;
                        xhr.send(data);
                    },
                    abort: function () {
                        jqXHR.abort();
                    }
                };
            }
        });
    }
}(jQuery || Zepto));
/**
 * Adds outer/innerHeight/Width methods to Zepto
 * https://gist.github.com/wheresrhys/5823198
 * Fixed and improved by Tomas J Stehlik (c) 2016
 * because Zepto now returns width and height using getBoundingClientRect which includes both, padding and border(!)
 * See http://help.dottoro.com/ljvmcrrn.php
 */
if (usingZepto()) {
    (function ($) {
        // Add inner and outer width to zepto (adapted from https://gist.github.com/alanhogan/3935463)
        var ioDim = function (dimension, includeBorder) {
            return function (includeMargin, excludePadding) {
                var sides, size, elem;
                if (this) {
                    elem = this;
                    size = elem[dimension]();
                    sides = {
                        width: ["left", "right"],
                        height: ["top", "bottom"]
                    };
                    sides[dimension].forEach(function (side) {
                        if (excludePadding) {
                            size -= parseInt(elem.css("padding-" + side), 10);
                        }
                        if (!includeBorder) {
                            size -= parseInt(elem.css("border-" + side + "-width"), 10);
                        }
                        if (includeMargin) {
                            size += parseInt(elem.css("margin-" + side), 10);
                        }
                    });
                    return size;
                } else {
                    return null;
                }
            }
        };
        ["width", "height"].forEach(function (dimension) {
            var Dimension = dimension.substr(0, 1).toUpperCase() + dimension.substr(1);
            $.fn["inner" + Dimension] = ioDim(dimension, false);
            $.fn["outer" + Dimension] = ioDim(dimension, true);
        });
    }(Zepto));
}
/**
 * See the following for the future:
 * https://gist.github.com/pamelafox/1379704
 */
function getWidth(el) {
    var out;
    if (el.length === 1) {
        if (usingZepto()) {
            out = el.innerWidth(false, true);
        } else {//jQuery
            // http://blog.jquery.com/2012/08/16/jquery-1-8-box-sizing-width-csswidth-and-outerwidth/
            out = parseFloat(el.css("width"));
        }
    }
    return out || 0;
}
function getHeight(el) {
    var out;
    if (el.length === 1) {
        if (usingZepto()) {
            out = el.innerHeight(false, true);
        } else {//jQuery
            // http://blog.jquery.com/2012/08/16/jquery-1-8-box-sizing-width-csswidth-and-outerwidth/
            out = parseFloat(el.css("height"));
        }
    }
    return out || 0;
}