var networkState,
    onLineOverride;
function initNetworkState() {
    setTimeout(function () {
        getNetworkState();
    }, 100);
}
function getNetworkState() {
    if (navigator.connection) {
        networkState = navigator.connection.type;
    }
    if (!networkState && navigator.network && navigator.network.connection) {
        networkState = navigator.network.connection.type;
    }
    return networkState;
}
function isOnline() {
    var out;
    if (typeof onLineOverride !== "undefined") {
        out = onLineOverride;
    } else {
        out = navigator.onLine;
    }
    return out;
}
function onOnline() {
    setTimeout(function () {
        logger("CONNECTED TO NETWORK" + (networkState === "none" ? "" : " (" + networkState + ")"), null, true);
        //triggerSync();
    }, 350);//wait for the initNetworkState
}
function onOffline() {
    setTimeout(function () {
        logger("DISCONNECTED FROM NETWORK" + (networkState === "none" ? "" : " (" + networkState + ")"), null, true);
        stopNetworkActivitySpinner();
    }, 350);//wait for the initNetworkState
}
function startNetworkActivitySpinner() {
    logger("network activity spinner started");
    if (typeof NetworkActivity === "object") {
        NetworkActivity.activityStart();
    }
}
function stopNetworkActivitySpinner() {
    logger("network activity spinner stopped");
    if (typeof NetworkActivity === "object") {
        NetworkActivity.activityStop();
    }
}
