window.settings = {};
function retrieveSettings(callback) {
    logger("retrieve settings");
    retrieveData([[
        "SELECT key, data FROM settings",
        function (tx, res) {
            var i, l, key, data;
            if (res.rows.length) {
                l = res.rows.length;
                for (i = 0; i < l; ++i) {
                    key = res.rows.item(i)["key"];
                    data = res.rows.item(i)["data"];
                    window.settings[key] = (isJson(data)
                        ? JSON.parse(data)
                        : data);
                }
            }
            //logger(window.settings, "settings");
            if (typeof callback === "function") {
                callback();
            }
        },
        queryFailed
    ]]);
}
function saveSetting(name, value, callback) {
    logger(name, "save setting");
    storeData([
        ["INSERT OR REPLACE INTO settings (key, data, tstamp) VALUES (?, ?, datetime('now'))", [name, value], callback]
    ]);
}