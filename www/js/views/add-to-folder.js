app.AddToFolderView = app.FolderAbstractView.extend({
    id: "add-to-folder-view-i",
    events: {
        "click .rli": "clickFolder",
        "click .edit-folders-button": "clickEditFolders",
        "swiperight": "hideAddToFolder",
        "swipeRight": "hideAddToFolder"
    },
    initialize: function () {
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _foldersList: null,
    _getCurrentRute: function () {
        return decodeURIComponent(window.location.hash.split("/").pop());
    },
    _refreshItemCount: function () {
        var self = this,
            $e,
            folderId,
            currentRute = this._getCurrentRute();
        //logger(self._foldersList, "_refreshItemCount, self._foldersList");
        //logger(window.foldersData.folder_data, "_refreshItemCount, window.foldersData.folder_data");
        self._foldersList.children().filter("li").each(function (i, e) {
            $e = $(e);
            folderId = $e[0].id;
            if (folderId) {
                $e.find(".item-count").first().text(self.getFolderItemsCount(folderId).toString());
                if (self.isRutePresentInFolder(folderId, currentRute)) {
                    $e.addClass("selected");
                } else {
                    $e.removeClass("selected");
                }
            }
        });
    },
    _addToFolder: function (item) {
        this.addFolderItem(item[0].id, this._getCurrentRute());
        this.storeFoldersData();
        this._refreshItemCount();
    },
    _removeFromFolder: function (item) {
        this.removeFolderItem(item[0].id, this._getCurrentRute());
        this.storeFoldersData();
        this._refreshItemCount();
    },
    _render: function () {
        var self = this;
        self.templateFromFile("templates/add-to-folder.html", self.$el, {
            data: window.foldersData,
            sort_order: self.getSortedFolderIds()
        }, function () {
            self._foldersList = self.$el.find("#add-to-folder").find("ul").first();
            self._refreshItemCount();
            _.delay(function () {
                self._foldersList.mobiscroll().listview({
                    theme: "ios-golf",
                    sortable: false,
                    iconSlide: true,
                    altRow: false,
                    stages: [{
                        //###################################
                        //## ADD TO
                        //###################################
                        percent: 20,
                        color: "green",
                        icon: "plus",
                        text: "Add to",
                        disabled: function (item) {
                            return self.isRutePresentInFolder(item[0].id, self._getCurrentRute());
                        },
                        action: function (item) {
                            self._addToFolder(item);
                        }
                    }, {
                        //###################################
                        //## REMOVE FROM
                        //###################################
                        percent: -20,
                        color: "red",
                        icon: "minus",
                        text: "Remove from",
                        disabled: function (item) {
                            return !self.isRutePresentInFolder(item[0].id, self._getCurrentRute());
                        },
                        action: function (item) {
                            self._removeFromFolder(item);
                        }
                    }],
                    onItemTap: function (item) {
                        if (self.isRutePresentInFolder(item[0].id, self._getCurrentRute())) {
                            self._removeFromFolder(item);
                        } else {
                            self._addToFolder(item);
                        }
                        app.main.hideAddToFolder();
                    }
                });
            }, 500);
            _.delay(function () {
                window.addToFolderIScroll = app.core.createIScroll("#add-to-folder-wrapper");
            }, 200);
            _.delay(self.callback, 150);
            _.delay(function () {
                if (window.useNativeAnimation && window.useNativeMenuAnimation) {
                    window["pluginsnativepagetransitions"]["executePendingTransition"]();
                }
                _.delay(function () {
                    window.addToFolderStatus = "opened";
                }, window.addToFolderTransitionDuration + 50);
            }, 100);
            logger("add to folder view drawing done");
        });
        return self;
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        var self = this;
        self.retrieveFolderData(function () {
            self._render();
        });
        return self;
    },
    clickFolder: _.debounce(function (e) {
        if (this._editingNow) {
            return false;//no click when editing, etc.
        }
        var listItem = $(e.target).closest(".rli"),
            data = listItem[0].id;
        if (data) {
            this._refreshItemCount();
        }
        return false;
    }, 300, true),
    clickEditFolders: function () {
        app.main.highlightClickedButton(".edit-folders-button");
        app.main.hideAddToFolder();
        _.delay(function () {
            window.location.hash = "#folders";
        }, 250);
    },
    hideAddToFolder: function (e) {
        //make sure we don't interfere with the mobiscroll events
        //we can't allow .rli items to close the add-to-folder in this case
        if (e.gesture.target.id === "add-to-folder-wrapper") {
            app.main.hideAddToFolder();
        }
        return false;
    },
    onOrientationChange: function () {
        app.core.refreshIScroll(window.addToFolderIScroll, 250);
    }
});