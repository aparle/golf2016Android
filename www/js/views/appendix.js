app.AppendixView = Backbone.View.extend({
    id: "appendix-view-i",
    initialize: function () {
        this.appendixId = 0;
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _appendix: {},
    _query1: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZAPPENDIXTITLE], [ZAPPENDIXTEXT] FROM [ZAPPENDIX] WHERE [Z_PK] = " + this.appendixId,
            {}, null,
            function (res) {
                self._appendix = res[0];
                self._appendix["ZAPPENDIXTEXT"] = trim(self._appendix["ZAPPENDIXTEXT"]);
                if (self._appendix["ZAPPENDIXTEXT"]) {
                    self._appendix["ZAPPENDIXTEXT"] = app.main.convertAppendixImages(self._appendix["ZAPPENDIXTEXT"]);
                    self._appendix["ZAPPENDIXTEXT"] = app.main.convertLinks(self._appendix["ZAPPENDIXTEXT"]);
                }
                self._render();
            }
        );
    },
    _render: function () {
        var self = this;
        if (!self._appendix) {//emergency
            self.$el.parent().hide();
            self.remove();
            logger("appendix view abandoned");
            return;
        }
        self.templateFromFile("templates/appendix.html", self.$el, {
            appendix: self._appendix
        }, function () {
            self.$el.find("img.image").magnificPopup({
                image: {
                    titleSrc: "title"
                },
                gallery: {
                    enabled: true,
                    preload: [1, 1],
                    navigateByImgClick: true
                }
            });
            _.delay(function () {
                window.appendixIScroll = app.core.createIScroll("#appendix-content-wrapper");
            }, 300);
            _.delay(self.callback, 150);
            logger("appendix view drawing done");
        });
    },
    render: function () {
        logger(this.appendixId, "appendix view", true);
        if (this.appendixId) {
            this._query1();
        }
        return this;
    },
    _removeIScroll: function () {
        if (window.appendixIScroll) {
            window.appendixIScroll.destroy();
            window.appendixIScroll = null;
        }
    },
    onOrientationChange: function () {
        _.delay(function () {
            app.core.refreshIScroll(window.appendixIScroll, 40);
        }, 300);
    }
});