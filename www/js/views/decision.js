app.DecisionView = Backbone.View.extend({
    id: "decision-view-i",
    className: "full-screen-view",
    events: {
        "click #decision-rule-title": "clickRule",
        "click #phone-view-decision-rule-holder": "clickRule",
        "click .d": "clickRelatedDecision",
        "click #phone-view-related-holder-i": "clickRelatedDecisionHolder",
        "swiperight": "decisionPrevious",//jQuery
        "swipeRight": "decisionPrevious",//Zepto
        "click .previous-item": "decisionPrevious",
        "swipeleft": "decisionNext",//jQuery
        "swipeLeft": "decisionNext",//Zepto
        "click .next-item": "decisionNext"
    },
    initialize: function () {
        this.rute = "";
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _decision: {},
    _rule: {},
    _related: {},
    _query1: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT * FROM [ZDECISION] WHERE [ZRUTE] = '" + self.rute + "'",
            this._decision,
            null,
            function (res) {
                self._decision = res[0];
                //title
                if (self._decision["ZDECISIONTITLE"]) {
                    self._decision["ZDECISIONTITLE"] = app.main.highlightFoundWords(self._decision["ZDECISIONTITLE"]);
                }
                //question
                self._decision["ZDECISIONQ"] = trim(self._decision["ZDECISIONQ"]);
                if (self._decision["ZDECISIONQ"]) {
                    self._decision["ZDECISIONQ"] = app.main.highlightFoundWords(self._decision["ZDECISIONQ"]);
                    self._decision["ZDECISIONQ"] = app.main.convertDecisionImage(self._decision["ZDECISIONQ"], self._decision["ZDECISIONIMG"], self._decision["ZDECISIONIMGTHUMB"], self._decision["ZDECISIONTITLE"]);
                    self._decision["ZDECISIONQ"] = app.main.convertLinks(self._decision["ZDECISIONQ"]);
                }
                //answer
                self._decision["ZDECISIONA"] = trim(self._decision["ZDECISIONA"]);
                if (self._decision["ZDECISIONA"]) {
                    self._decision["ZDECISIONA"] = app.main.highlightFoundWords(self._decision["ZDECISIONA"]);
                    self._decision["ZDECISIONA"] = app.main.convertDecisionImage(self._decision["ZDECISIONA"], self._decision["ZDECISIONIMG"], self._decision["ZDECISIONIMGTHUMB"], self._decision["ZDECISIONTITLE"]);
                    self._decision["ZDECISIONA"] = app.main.convertLinks(self._decision["ZDECISIONA"]);
                }
                //for sharing
                window.sharingSubject = "Decision:" + self._decision["ZRUTE"];
                window.sharingMessage = "Q:" +
                    "\n" +
                    app.main.prepareMessageForSharing(self._decision["ZDECISIONQ"]) +
                    "\n\n" +
                    "A:" +
                    "\n" +
                    app.main.prepareMessageForSharing(self._decision["ZDECISIONA"]);
                self._query2();
            }
        );
    },
    _query2: function () {
        var sql,
            self = this,
            ruleRute = app.main.removeLettersAfterTheSubRuleDigits(self._decision["ZDECISIONNUMBERMAJOR"] || self._decision["ZRUTE"]);
        if (ruleRute.indexOf("M") === 0) {//special case, handicaps, rule 35 is called "M"
            sql = "SELECT * FROM [ZMAINRULE] WHERE [ZRULENUM] = '35'";
        } else if (ruleRute.indexOf("-") >= 0) {
            sql = "SELECT * FROM [ZSUBRULE] WHERE [ZRUTE] = '" + ruleRute + "'";
        } else {
            sql = "SELECT * FROM [ZMAINRULE] WHERE [ZRULENUM] = '" + ruleRute + "'";
        }
        queryAndProcessSqlResult(
            sql,
            this._rule,
            null,
            function (res) {
                self._rule = res[0];
                self._rule["ZMAINRULESTXT"] = trim(self._rule["ZMAINRULESTXT"]);
                self._rule["ZSUBRULESTEXT"] = trim(self._rule["ZSUBRULESTEXT"]);
                if (self._rule["ZMAINRULESTXT"]) {
                    self._rule["ZMAINRULESTXT"] = app.main.convertLinks(self._rule["ZMAINRULESTXT"]);
                }
                else if (self._rule["ZSUBRULESTEXT"]) {
                    self._rule["ZSUBRULESTEXT"] = app.main.convertLinks(self._rule["ZSUBRULESTEXT"]);
                }
                self._query3();
            }
        );
    },
    _query3: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZDECISIONTITLE], [ZRUTE] " +
            "FROM [ZRELATEDDECISION] " +
            "WHERE [ZMAINDECISIONID] = '" + self._decision["ZDECISIONID"] + "' " +
            "ORDER BY [ZORDER]",
            this._related,
            null,
            function (res) {
                self._related = res;
                self._render();
            }
        );
    },
    _render: function () {
        var self = this;
        if (!self._decision) {//emergency
            self.$el.parent().hide();
            self.remove();
            logger("decision view abandoned");
            return;
        }
        self.templateFromFile("templates/decision.html", self.$el, {
            decision: self._decision,
            rule: self._rule,
            related: self._related
        }, function () {
            _relatedCount = _.size(self._related);
            _setDecisionContentWrappersHeights(self.$el, _relatedCount);
            app.main.highlightListItemByRetrieval();
            self.$el.find("img.image").magnificPopup({
                callbacks: {
                    //style='max-width: none;' 

                    open: function () {
                        // // cordova.InAppBrowser.open('img/media/1-1_3.888.jpg','_blank','location=no,enableViewportScale=true');
                        logger("open");
                        unlockOrientation();
                        // var self = this;
                        // self.content.addClass('popDecisionImage');
                        // self.wrap.toggleClass('mfp-force-scrollbars');
                    },
                    beforeClose: function () {
                        // var self = this;
                        // self.content.removeClass('popDecisionImage');
                        // self.wrap.removeClass('mfp-force-scrollbars');
                        logger("beforeClose");
                        setPhoneViewIfNecessary();
                    }
                },
                image: {
                    titleSrc: "title"
                },
                gallery: {
                    enabled: false,
                    preload: [1, 1],
                    navigateByImgClick: true
                }
            });
            _.delay(function () {
                app.main.replaceImgBySvg(self.$el.find(".phone-view-holder-button"));
                app.main.replaceImgBySvg(self.$el.find("#footer-view"));
            }, 80);
            if (window.isPhoneView) {
                $("#related").addClass("holder close").appendTo("#phone-view-related-holder");
            }
            _.delay(function () {
                window.decisionIScroll = app.core.createIScroll("#decision-content-wrapper");
                if (!window.isPhoneView) {
                    window.decisionRuleIScroll = app.core.createIScroll("#decision-rule-content-wrapper");
                    if (_relatedCount) {
                        window.relatedIScroll = app.core.createIScroll("#related-content-wrapper");
                    }
                }
            }, 300);
            _.delay(spinnerDialogHide, 90);
            _.delay(self.callback, 150);
            logger("decision view drawing done");
        });
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        logger(this.rute, "decision view", true);
        if (this.rute) {
            this._query1();
        }
        return this;
    },
    _removeIScroll: function () {
        if (window.decisionIScroll) {
            window.decisionIScroll.destroy();
            window.decisionIScroll = null;
        }
        if (!window.isPhoneView) {
            if (window.decisionRuleIScroll) {
                window.decisionRuleIScroll.destroy();
                window.decisionRuleIScroll = null;
            }
            if (_relatedCount && window.relatedIScroll) {
                window.relatedIScroll.destroy();
                window.relatedIScroll = null;
            }
        }
    },
    clickRule: function (e) {
        //app.core.logEvent(e);
        var el = $(e.target).closest("div"),
            data = el.data("rute"),
            arrowRight = el.find(".arrow-right");
        if (arrowRight.length > 0) {
            arrowRight.addClass("clicked");
        } else {
            el.addClass("clicked");
        }
        if (data) {
            this._removeIScroll();
            _.delay(function () {
                app.main.setTemporaryOptionsForLateralMovements();
                //spinnerDialogShow();
                window.location.hash = "#rule/" + encodeURIComponent(data);
            }, 30);
        }
        return false;
    },
    clickRelatedDecision: function (e) {
        //app.core.logEvent(e);
        var listItem = $(e.target).closest(".rli"),
            data = listItem.data("rute");
        if (data) {
            var self = this,
                hash = "#decision/" + encodeURIComponent(data);
            if (window.location.hash !== hash) {
                app.main.highlightListItemByAddition(listItem, function () {
                    self._removeIScroll();
                    _.delay(function () {
                        app.main.setTemporaryOptionsForLateralMovements();
                        //spinnerDialogShow();
                        window.location.hash = hash;
                    }, 30);
                });
            }
        }
        return false;
    },
    clickRelatedDecisionHolder: function () {
        app.main.toggleHolder(this, $("#phone-view-related-holder-i"), $("#related"));
        return false;
    },
    decisionPrevious: function () {
        window.decisionsSequence.setCur(_.indexOf(window.decisionsSequence, this.rute));
        var decision = window.decisionsSequence.prev();
        if (decision) {
            app.main.highlightClickedButton(".previous-item");
            this._removeIScroll();
            _.delay(function () {
                window.historyBackDetected = true;
                app.main.setTemporaryOptionsForLateralMovements();
                //spinnerDialogShow();
                app.router.navigateCustom("#decision/" + encodeURIComponent(decision), {
                    trigger: true,
                    replace: true
                });
            }, 30);
        }
        return false;
    },
    decisionNext: function () {
        window.decisionsSequence.setCur(_.indexOf(window.decisionsSequence, this.rute));
        var decision = window.decisionsSequence.next();
        if (decision) {
            app.main.highlightClickedButton(".next-item");
            this._removeIScroll();
            _.delay(function () {
                app.main.setTemporaryOptionsForLateralMovements();
                //spinnerDialogShow();
                app.router.navigateCustom("#decision/" + encodeURIComponent(decision), {
                    trigger: true,
                    replace: true
                });
            }, 30);
        }
        return false;
    },
    onOrientationChange: function () {
        _.delay(function (view) {
            logger(view, "onOrientationChange, view");
            _setDecisionContentWrappersHeights(view.$el, _relatedCount);
            app.core.refreshIScroll(window.decisionIScroll, 50);
            if (!window.isPhoneView) {
                app.core.refreshIScroll(window.decisionRuleIScroll, 60);
                if (_relatedCount) {
                    app.core.refreshIScroll(window.relatedIScroll, 70);//more time for images to load
                }
            }
        }, 300, this);
    }
});
var _relatedCount = 0,
    _setDecisionContentWrappersHeights = function (viewEl, relatedCount) {
        var decisionViewWithoutFooter = $("#decision-view-i2");
        viewEl = viewEl || decisionViewWithoutFooter;
        var decision = viewEl.find("#decision"),
            decisionMarginTop = Math.round(parseFloat(decision.css("margin-top").replace("px", ""))),
            decisionTitleHeight = Math.round(viewEl.find("#decision-title").outerHeight()),
            height = Math.round(parseFloat(getHeight(decisionViewWithoutFooter))) - (decisionTitleHeight + (2 * decisionMarginTop));
        viewEl.find("#decision-content-wrapper").height(height);
        if (window.isPhoneView) {
            $("#decision-title-bg").height(decisionTitleHeight + decisionMarginTop);
            return;//we don't need the rest in the phone view
        }
        var borderWidth = 1,
            sideHeight = height,
            rule = viewEl.find("#decision-rule"),
            relatedTitleHeight,
            related,
            numberOfBoxes = 1,
            sideScrollHeight;
        if (relatedCount) {
            relatedTitleHeight = Math.round(viewEl.find("#related-title").outerHeight());
            related = viewEl.find("#related");
            sideHeight -= (relatedTitleHeight + Math.round(parseFloat(related.css("margin-top").replace("px", ""))));
            numberOfBoxes += 1;
        }
        if (numberOfBoxes) {
            sideHeight = Math.floor(sideHeight / numberOfBoxes);
            rule.height(decisionTitleHeight + sideHeight);
            if (relatedCount) {
                related.height(relatedTitleHeight + sideHeight);
            }
            sideScrollHeight = sideHeight - (2 * borderWidth);
            viewEl.find(".scroll-content-border").height(sideScrollHeight);
            viewEl.find(".side-content-wrapper").height(sideScrollHeight);
        }
    };
