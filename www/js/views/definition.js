app.DefinitionView = Backbone.View.extend({
    id: "definition-view-i",
    initialize: function () {
        this.definition = "";
        return this;
    },
    _definition: {},
    _query1: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZTITLE], [ZDEFINITIONTXT] FROM [ZDEFINITION] WHERE [ZTITLE] LIKE '%" + this.definition + "%'",
            {}, null,
            function (res) {
                self._definition = _.find(res, function (row) {//find the exactly matching definition
                    return ($.inArray(trimLower(self.definition), _.map(row['ZTITLE'].split(","), trimLower)) !== -1);
                });
                if (self._definition["ZTITLE"]) {
                    //use only the first term before the first comma
                    self._definition["ZTITLE"] = self._definition["ZTITLE"].split(",")[0];
                }
                if (self._definition["ZDEFINITIONTXT"]) {
                    self._definition["ZDEFINITIONTXT"] = app.main.convertLinks(self._definition["ZDEFINITIONTXT"]);
                }
                self._render();
            }
        );
    },
    _render: function () {
        var self = this;
        this.templateFromFile("templates/definition.html", self.$el, {
            definition: self._definition
        }, function () {
            var calculatingNow = false,
                calculateHeight = function () {
                    if (calculatingNow) {
                        return;//don't start multiple recalculations!
                    }
                    calculatingNow = true;
                    _.delay(function () {
                        var height = getHeight($(".mfp-content").first()),
                            dcw = $("#definition-content-wrapper"),
                            iScrollPresent = !!(dcw.find(".iScrollIndicator").length);
                        //logger(height, "definition, height", true);
                        dcw.height(height);
                        if (iScrollPresent) {
                            app.core.refreshIScroll(window.definitionIScroll, 250);
                        } else {
                            createIScroll(50);
                        }
                        calculatingNow = false;
                    }, 150);
                },
                createIScroll = function (delay) {
                    _.delay(function () {
                        window.definitionIScroll = app.core.createIScroll("#definition-content-wrapper");
                    }, delay || 200);
                    $(window).on("orientationchange", self.onOrientationChange);
                },
                removeView = function () {
                    self.$el.hide();
                    $(window).off("orientationchange", self.onOrientationChange);
                    if (window.definitionIScroll) {
                        window.definitionIScroll.destroy();
                        window.definitionIScroll = null;
                    }
                    self.remove();
                };
            if (self._definition) {
                $.magnificPopup.open({
                    items: {
                        src: "#definition-view"
                    },
                    gallery: {
                        enabled: true
                    },
                    callbacks: {
                        open: calculateHeight,
                        resize: calculateHeight,
                        change: calculateHeight,
                        close: removeView
                    },
                    closeBtnInside: true
                });
                logger("definition view drawing done");
            } else {
                removeView();
                logger("nothing to display, abandoning definition view");
            }
        });
    },
    render: function () {
        logger(this.definition, "definition view", true);
        if (this.definition) {
            this._query1();
        }
        return this;
    },
    onOrientationChange: function () {
        _.delay(function () {
            app.core.refreshIScroll(window.definitionIScroll, 40);
        }, 300);
    }
});