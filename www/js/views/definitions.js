app.DefinitionsView = Backbone.View.extend({
    id: "definitions-view-i",
    initialize: function () {
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _definitions: {},
    _query1: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZTITLE], [ZDEFINITIONTXT] FROM [ZDEFINITION] ORDER BY [ZTITLE]",
            {}, null,
            function (res) {
                self._definitions = res;
                _.each(self._definitions, function (definition, key) {
                    if (self._definitions[key]["ZTITLE"]) {
                        //use only the first term before the first comma
                        self._definitions[key]["ZTITLE"] = definition["ZTITLE"].split(",")[0];
                    }
                    if (self._definitions[key]["ZDEFINITIONTXT"]) {
                        self._definitions[key]["ZDEFINITIONTXT"] = app.main.convertLinks(definition["ZDEFINITIONTXT"]);
                    }
                });
                self._render();
            }
        );
    },
    _render: function () {
        var self = this;
        this.templateFromFile("templates/definitions.html", self.$el, {
            definitions: self._definitions
        }, function () {
            _.delay(function () {
                window.definitionsIScroll = app.core.createIScroll("#definitions-content-wrapper");
            }, 200);
            _.delay(self.callback, 150);
            logger("definitions view drawing done");
        });
    },
    render: function () {
        this._query1();
        return this;
    },
    _removeIScroll: function () {
        if (window.definitionsIScroll) {
            window.definitionsIScroll.destroy();
            window.definitionsIScroll = null;
        }
    },
    onOrientationChange: function () {
        _.delay(function () {
            app.core.refreshIScroll(window.definitionsIScroll, 40);
        }, 300);
    }
});