app.FolderRulesAndDecisionsListView = app.FolderAbstractView.extend({
    id: "folder-rules-and-decisions-list-view-i",
    className: "full-screen-view",
    events: {
        "click .r": "clickRule",
        "click .d": "clickDecision",
        "swiperight": "openMenu",
        "swipeRight": "openMenu"
    },
    initialize: function () {
        this.folderId = "";
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _getCurrentFolderId: function () {
        return decodeURIComponent(window.location.hash.split("/").pop());
    },
    _removeFromFolder: function (item) {
        this.removeFolderItem(this._getCurrentFolderId(), item[0].dataset.rute);
        this.storeFoldersData();
    },
    _render: function () {
        var self = this;
        this.templateFromFile("templates/folder-rules-and-decisions-list.html", self.$el, {
            mainRules: window.cacheMainRules,
            subRules: window.cacheSubRules,
            decisions: window.cacheDecisions,
            folderItems: self.getSortedFolderItemsIds(self.folderId),
            folderName: self.getFolderName(self.folderId)
        }, function () {
            app.main.highlightListItemByRetrieval();
            _.delay(function () {
                self.$el.find("#folder-rules-and-decisions-list").find("ul").first().mobiscroll().listview({
                    theme: "ios-golf",
                    sortable: false,
                    iconSlide: true,
                    altRow: false,
                    swipe: "left",
                    stages: [{
                        //###################################
                        //## REMOVE FROM
                        //###################################
                        percent: -50,
                        color: "red",
                        icon: "minus",
                        text: "Remove from this folder",
                        action: function (item, inst) {
                            self._removeFromFolder(item);
                            inst.remove(item);
                            self.onOrientationChange();
                            return false;
                        }
                    }]
                });
            }, 500);
            _.delay(function () {
                window.folderRulesAndDecisionsListIScroll = app.core.createIScroll("#folder-rules-and-decisions-list-wrapper", true);
            }, 200);
            _.delay(self.callback, 150);
            logger("folder rules and decisions list view drawing done");
        });
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        var self = this;
        self.retrieveFolderData(function () {
            self._render();
        });
        return self;
    },
    _click: _.debounce(function (e, type) {
        //app.core.logEvent(e);
        if (type !== "rule" && type !== "decision") {
            logger(type, "rules and decisions list, _click, incorrect type supplied", true);
            return;
        }
        var listItem = $(e.target).closest(".rli"),
            data = listItem.data("rute");
        if (data) {
            app.main.highlightListItemByAddition(listItem, function () {
                _.delay(function () {
                    //spinnerDialogShow();
                    window.location.hash = "#" + type + "/" + encodeURIComponent(data);
                }, 100);
            });
        }
    }, 300, true),
    clickRule: function (e) {
        this._click(e, "rule");
        return false;
    },
    clickDecision: function (e) {
        this._click(e, "decision");
        return false;
    },
    openMenu: function () {
        app.main.showMenu();
        return false;
    },
    onOrientationChange: function () {
        app.core.refreshIScroll(window.folderRulesAndDecisionsListIScroll, 250);
    }
});