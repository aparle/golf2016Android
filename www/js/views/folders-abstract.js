app.FolderAbstractView = Backbone.View.extend({
    writeToFile: function (fileName, data, successCallback, errorCallback) {
        data = JSON.stringify(data, null, "\t");
        window.resolveLocalFileSystemURL(
            cordova.file.syncedDataDirectory || cordova.file.dataDirectory,
            function (de) {
                de.getFile(fileName, {create: true}, function (fe) {
                    fe.createWriter(function (fw) {
                        fw.onwriteend = successCallback;
                        fw.onerror = errorCallback || logger;
                        fw.write(new Blob([data], {type: 'text/plain'}));
                    }, errorCallback || logger);
                }, errorCallback || logger);
            }, errorCallback || logger);
    },
    readFromFile: function (fileName, successCallback, errorCallback) {
        window.resolveLocalFileSystemURL(
            (cordova.file.syncedDataDirectory || cordova.file.dataDirectory) + fileName,
            function (fe) {
                fe.file(function (f) {
                    var r = new FileReader();
                    r.onloadend = function () {
                        if ($.isFunction(successCallback)) {
                            successCallback(JSON.parse(this.result));
                        }
                    };
                    r.readAsText(f);
                }, errorCallback || logger);
            }, errorCallback || logger);
    },
    storeFoldersData: function () {
        //logger(window.foldersData, "folder list, data");
        this.writeToFile(window.foldersDataFileName, window.foldersData);
    },
    storeUndoCopyOfFoldersData: function () {
        window.foldersData.undoCopy = {};
        window.foldersData.undoCopy.folders = $.extend(true, {}, window.foldersData.folders);
        window.foldersData.undoCopy.sort_order = $.extend(true, {}, window.foldersData.sort_order);
        window.foldersData.undoCopy.folder_data = $.extend(true, {}, window.foldersData.folder_data);
        this.storeFoldersData();
    },
    retrieveFolderData: function (callback) {
        var self = this,
            createNew = function () {
                self.addNewFolder("Folder 1");
                //for (var i = 2; i < 16; i++) {
                //    self.addNewFolder("Folder " + i.toString());
                //}
            };
        self.readFromFile(window.foldersDataFileName, function (data) {
            //logger(data, "folder list, data");
            if (data && !$.isEmptyObject(data)) {
                window.foldersData = data;
            } else {
                createNew();
            }
            if ($.isFunction(callback)) {
                callback();
            }
        }, function () {
            createNew();
            if ($.isFunction(callback)) {
                callback();
            }
        });
    },
    generateUniqueFolderId: function () {
        return "f" + (crc32(Date.now() + Math.random().toString()) >>> 0).toString(16);
    },
    addNewFolder: function (name, sortOrder) {
        var fid = this.generateUniqueFolderId(),
            data = $.isEmptyObject(window.foldersData)
                ? {folders: {}, sort_order: {}, folder_data: {}}
                : window.foldersData;
        logger(fid, "new folder id");
        data.folders[fid] = name;
        data.sort_order[fid] = sortOrder || 0;
        data.folder_data[fid] = {};
        window.foldersData = data;
        this.storeFoldersData();
        return fid;
    },
    deleteFolder: function (id) {
        delete(window.foldersData.folders[id]);
        delete(window.foldersData.sort_order[id]);
        delete(window.foldersData.folder_data[id]);
        this.storeFoldersData();
    },
    updateFolderName: function (id, name) {
        window.foldersData.folders[id] = name;
    },
    updateFolderSortOrder: function (id, sortOrder) {
        window.foldersData.sort_order[id] = sortOrder;
    },
    updateFolderData: function (id, type, key, value) {
        if (window.foldersData.folder_data[id] === undefined) {
            window.foldersData.folder_data[id] = {};
        }
        if (window.foldersData.folder_data[id][type] === undefined) {
            window.foldersData.folder_data[id][type] = {};
        }
        window.foldersData.folder_data[id][type][key] = value;
    },
    removeFolderData: function (id, type, key) {
        if (window.foldersData.folder_data[id]
            && window.foldersData.folder_data[id][type]
            && window.foldersData.folder_data[id][type][key]) {
            delete(window.foldersData.folder_data[id][type][key]);
        }
    },
    addFolderItem: function (id, rute) {
        this.updateFolderData(id, "items", rute, app.main.makeSortableDecisionId(rute));
    },
    removeFolderItem: function (id, rute) {
        this.removeFolderData(id, "items", rute);
    },
    getFolderName: function (id) {
        return window.foldersData.folders[id];
    },
    getFolderItemsCount: function (id) {
        //logger(window.foldersData.folder_data[id]["items"], "getFolderItemsCount, items of " + id);
        return (window.foldersData.folder_data[id] && window.foldersData.folder_data[id]["items"])
            ? _.size(window.foldersData.folder_data[id]["items"])
            : 0;
    },
    isRutePresentInFolder: function (id, rute) {
        //logger(window.foldersData.folder_data[id]["items"][rute], "isRutePresentInFolder, " + id + ", " + rute);
        return (
            window.foldersData.folder_data[id]
            && window.foldersData.folder_data[id]["items"]
            && window.foldersData.folder_data[id]["items"][rute]
        );
    },
    getSortedFolderIds: function () {
        var out = [],
            list = _.pairs(window.foldersData.sort_order);
        if (_.size(list)) {
            list.sort(function (a, b) {
                return a[1] - b[1];
            });
            _.each(list, function (e) {
                out.push(e[0]);
            });
        }
        return out;
    },
    getSortedFolderItemsIds: function (folderId) {
        var out = [],
            list = _.pairs(window.foldersData.folder_data[folderId]["items"]);
        if (_.size(list)) {
            //logger(list, "getSortedFolderItemsIds, list");
            var res = _.sortBy(list, 1);
            _.each(res, function (e) {
                out.push(e[0]);
            });
            //logger(res, "getSortedFolderItemsIds, res");
            //logger(out, "getSortedFolderItemsIds, out");
        }
        return out;
    }
});