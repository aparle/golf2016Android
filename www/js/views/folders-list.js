app.FoldersListView = app.FolderAbstractView.extend({
    id: "folders-list-view-i",
    className: "full-screen-view",
    events: {
        "click .rli": "clickFolder",
        "keyup": "keyUpInput",
        "swiperight": "openMenu",
        "swipeRight": "openMenu"
    },
    initialize: function () {
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _scrollTo: function (id, delay) {
        _.delay(function () {
            if (window.useIScroll && window.foldersListIScroll) {
                window.foldersListIScroll.scrollToElement("#" + id, 0);
            } else {//in case the iScroller is not present, we need to do this "manually"
                document.getElementById("folders-list-wrapper").scrollTop = document.getElementById(id).offsetTop;
            }
        }, delay || 500);
    },
    _editingNow: false,
    _editingFolderId: "",
    _editingFolderEl: null,
    _makeEditable: function (item) {
        //logger(item, "_makeEditable, item");
        var self = this,
            el = item.find(".editable"),
            originalValue = el.val(),
            folderId = item[0].id,
            finishEditing = function () {
                item.parent().find(".br2r").remove();//remove line break
                self._editingNow = false;//re-enable onclick event
                self._editingFolderId = "";//nothing is being edited now
                self._editingFolderEl = null;//ditto
            };
        self._editingNow = true;//temporarily onclick event for the sake of editing
        self._editingFolderId = folderId;
        self._editingFolderEl = item;
        el.editable(function (value) {//what to do with the submitted value
            //logger(value, "editable, value");
            //logger(settings, "editable, settings");
            if (value === originalValue) {
                logger("editable, no update");
            } else {//only update if there was a change
                self.updateFolderName(folderId, value);
                self.storeFoldersData();
            }
            return value;
        }, {
            event: "programmatic",
            onblur: "submit",
            onreset: finishEditing,
            onsubmit: finishEditing,
            cancel: 'Cancel',
            submit: 'OK',
            style: "inherit"//display: inline;
        }).trigger("programmatic");
        _.delay(function () {
            var input = el.find("form").find("input").first();
            input.after($("<br/>").addClass("br2r"));//add a line break
            var onKeyboardShow = function () {
                logger("onKeyboardShow");
                _.delay(function () {
                    input.focus();
                }, 100);
                window.removeEventListener('native.keyboardshow', onKeyboardShow);
            };
            window.addEventListener('native.keyboardshow', onKeyboardShow);
            cordova.plugins.Keyboard.show();
        }, 100);
    },
    _render: function () {
        var self = this;
        self.templateFromFile("templates/folders-list.html", self.$el, {
            data: window.foldersData,
            sort_order: self.getSortedFolderIds()
        }, function () {
            app.main.highlightListItemByRetrieval();
            var foldersList = self.$el.find("#folders-list").find("ul").first(),
                getListSize = function () {
                    return foldersList.children().filter("li").length;
                },
                renumberReSortedList = function () {
                    var $e,
                        sortOrderNumber,
                        folderId;
                    foldersList.children().filter("li").each(function (i, e) {
                        $e = $(e);
                        sortOrderNumber = i + 1;
                        $e.find(".rn").first().text(sortOrderNumber);
                        folderId = $e[0].id;
                        //logger(folderId, "renumberReSortedList, folderId");
                        if (folderId) {
                            self.updateFolderSortOrder(folderId, sortOrderNumber);
                            $e.find(".item-count").first().text(self.getFolderItemsCount(folderId));
                        }
                    });
                    self.storeFoldersData();//persist the new sort order
                };
            renumberReSortedList();
            foldersList.mobiscroll().listview({
                theme: "ios-golf",
                sortable: true,
                iconSlide: true,
                altRow: false,
                stages: [{
                    percent: 95,
                    color: "#284379",
                    icon: "smiley2",
                    text: "Hello",
                    disabled: function () {
                        return false;
                    },
                    action: function () {
                        return true;
                    }
                }, {
                    //###################################
                    //## EDIT
                    //###################################
                    percent: 50,
                    color: "blue",
                    icon: "pencil",//material-edit
                    text: "Edit",
                    disabled: function () {
                        return self._editingNow;//can't edit when already editing
                    },
                    action: function (item) {
                        self._makeEditable(item);
                    }
                }, {
                    //###################################
                    //## ADD
                    //###################################
                    percent: 25,
                    color: "green",
                    icon: "plus",//material-add-circle
                    text: "Add",
                    disabled: function () {
                        return self._editingNow;//can't add when editing
                    },
                    action: function (item, inst, index) {
                        var newFolderName = "Folder " + (getListSize() + 1),
                            newFolderId = self.addNewFolder(newFolderName),
                            newFolderEl;
                        inst.add(
                            newFolderId,
                            "<div class='rn'></div>" +
                            "<span class='item-count'>0</span>" +
                            "<span class='editable'>" + newFolderName + "</span>",
                            index + 1
                        );
                        newFolderEl = item.closest("ul").first().find("li[data-id=" + newFolderId + "]").first()
                            .addClass("rli").attr("id", newFolderId);
                        renumberReSortedList();
                        self._makeEditable(newFolderEl);
                    }
                }, {
                    //###################################
                    //## DELETE
                    //###################################
                    percent: -25,
                    color: "red",
                    icon: "remove",//material-delete
                    text: "Delete",
                    confirm: function (item) {
                        return (self.getFolderItemsCount(item[0].id) > 0);//no confirmation for empty folders
                    },
                    disabled: function () {
                        return self._editingNow || (getListSize() === 1);//can't delete the last folder or when editing
                    },
                    action: function (item, inst) {
                        logger(item[0].id, "delete folder id");
                        self.storeUndoCopyOfFoldersData();
                        self.deleteFolder(item[0].id);
                        inst.remove(item, null, renumberReSortedList);
                        self.onOrientationChange();
                        return false;
                    }
                }, {
                    percent: -65,
                    color: "orange",
                    icon: "thumbs-up2",
                    text: "Oops! A bit too far...",
                    disabled: function () {
                        return false;
                    },
                    action: function () {
                        return true;
                    }
                }],
                onSortStart: function (item) {
                    item.find(".rn").first().text("0");//no sort number during the move
                },
                onSortStop: function () {
                    logger("folder list sorted");
                    renumberReSortedList();
                }
            });
            _.delay(function () {
                window.foldersListIScroll = app.core.createIScroll("#folders-list-wrapper");
            }, 200);
            _.delay(self.callback, 150);
            logger("folders list view drawing done");
        });
        return this;
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        var self = this;
        self.retrieveFolderData(function () {
            self._render();
        });
        return this;
    },
    _resetFoldersData: function () {
        window.foldersData = {};
        self.storeFoldersData();
    },
    clickFolder: _.debounce(function (e) {
        if (this._editingNow) {
            return false;//no click when editing, etc.
        }
        var listItem = $(e.target).closest(".rli"),
            data = listItem[0].id;
        if (data) {
            app.main.highlightListItemByAddition(listItem, function () {
                _.delay(function () {
                    //spinnerDialogShow();
                    window.location.hash = "#folder/" + encodeURIComponent(data);
                }, 30);
            });
        }
        return false;
    }, 300, true),
    keyUpInput: function (e) {
        var code = e.keyCode || e.which;
        if (code === 13 || code === 10) {
            logger("keyUpInput, enter pressed");
            if (this._editingNow && this._editingFolderEl.length > 0) {
                var el = this._editingFolderEl.find("#edit-submit").first();
                if (el.length > 0) {
                    var onKeyboardHide = function () {
                        logger("onKeyboardHide");
                        el.focus();
                        _.delay(function () {
                            window.removeEventListener('native.keyboardhide', onKeyboardHide);
                        }, 100);
                    };
                    window.addEventListener('native.keyboardhide', onKeyboardHide);
                    cordova.plugins.Keyboard.close();
                }
            }
            return false;
        }
    },
    openMenu: function (e) {
        //make sure we don't interfere with the mobiscroll events
        //we can't allow .rli items to open menu in this case
        if (e.gesture.target.id === "folders-list-wrapper") {
            app.main.showMenu();
        }
        return false;
    },
    onOrientationChange: function () {
        if (this._editingNow) {
            this._scrollTo(this._editingFolderId, 100);
        }
        app.core.refreshIScroll(window.foldersListIScroll, 250);
    }
});