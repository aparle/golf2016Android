app.HeaderView = Backbone.View.extend({
    events: {
        "click .back-button": "clickBack",
        "click .home-button": "clickHome",
        "click .share-button": "clickShare",
        "click .folder-button": "clickFolder",
        "click .explore-button": "clickExplore",
        "click .search-button": "clickSearch",
        "click .menu-button": "clickMenu"
    },
    render: function () {
        var self = this,
            previousKind = app.main.getKindFromHistoryStack(1),
            thisKind = window.location.hash.split("/")[0].replace(/^#/, ""),
            headerNav,
            headerTitle;
        //logger(kind, "header, kind", true);
        //logger(getWidth($("#header-view")), "header, view, width", true);
        switch (previousKind) {
            case "rulesAndDecisionsList":
            case "rulesList":
            case "subRulesList":
            case "folders":
            case "folder":
            case "videosList":
            case "page":
            case "overview":
                headerNav = "Back";
                break;
            case "rule":
                headerNav = "Rules";
                break;
            case "decision":
                headerNav = "Decisions";
                break;
            case "definitions":
                headerNav = "Definitions";
                break;
            case "appendix":
                headerNav = "Appendix";
                break;
            default:
                headerNav = "";//menu will be served
                break;
        }
        switch (thisKind) {
            case "rulesAndDecisionsList":
                headerTitle = "Rules and Decisions";
                break;
            case "rulesList":
                headerTitle = "Rules of Golf";
                break;
            case "subRulesList":
                headerTitle = "Rule";
                break;
            case "folders":
                headerTitle = "My Folders";
                break;
            case "folder":
                headerTitle = "My Decisions";
                break;
            case "videosList":
                headerTitle = "Videos";
                break;
            case "rule":
                headerTitle = window.isPhoneView ? "" : "Rules";
                break;
            case "decision":
                headerTitle = window.isPhoneView ? "" : "Decisions";
                break;
            case "page":
                headerTitle = "";
                break;
            default:
                headerTitle = thisKind
                    ? (thisKind.charAt(0).toUpperCase() + thisKind.slice(1))//upper case first
                    : "";
                break;
        }
        self.templateFromFile("templates/header.html", self.$el, {
            headerNav: headerNav,
            headerTitle: headerTitle,
            isRulesAndDecisionsListView: (thisKind === "rulesAndDecisionsList"),
            isRuleViewOrDecisionView: (thisKind === "rule" || thisKind === "decision"),
            isNarrowScreen: (getWidth($("#header-view")) < 400)
        }, function () {
            _.delay(function () {
                app.main.replaceImgBySvg(self.$el);
            }, 80);
            logger("header view drawing done");
        });
        return self;
    },
    clickBack: function () {
        app.main.highlightClickedButton(".back-button");
        _.delay(function () {
            app.main.goBackInHistoryStack();
        }, 30);
        return false;
    },
    clickHome: function () {
        app.main.highlightClickedButton(".home-button");
        _.delay(function () {
            if ("#rulesAndDecisionsList" !== window.location.hash) {
                window.location.hash = "#rulesAndDecisionsList";
            }
        }, 30);
        return false;
    },
    clickShare: function () {
        app.main.highlightClickedButton(".share-button");
        app.main.showSharing();
        return false;
    },
    clickFolder: function () {
        app.main.highlightClickedButton(".folder-button");
        app.main.showAddToFolder();
        return false;
    },
    clickExplore: function () {
        app.main.highlightClickedButton(".explore-button");
        if (window.exploreDecisionsOn) {
            window.exploreDecisionsOn = false;
            app.main.hideExploreDecisions();
        } else {
            window.exploreDecisionsOn = true;
            app.main.showExploreDecisions();
        }
        return false;
    },
    clickSearch: function () {
        app.main.highlightClickedButton(".search-button");
        //navigator.notification.alert("Search Decisions feature hasn't been implemented yet.");
        var weAreHere = ("#rulesAndDecisionsList" === window.location.hash);
        _.delay(function () {
            if (window.searchOn) {
                window.searchOn = false;
                app.main.hideSearch();
            } else {
                window.searchOn = true;
                app.main.showSearch();
            }
        }, weAreHere ? 50 : 400);
        if (!weAreHere) {
            _.delay(function () {
                window.location.hash = "#rulesAndDecisionsList";
            }, 30);
        }
        return false;
    },
    clickMenu: function () {
        app.main.highlightClickedButton(".menu-button");
        app.main.showMenu();
        return false;
    }
});