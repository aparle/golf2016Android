app.MenuView = Backbone.View.extend({
    id: "menu-view-i",
    events: {
        "click .menu-button-overview": "clickOverview",
        "click .menu-button-search-decisions": "clickSearchDecisions",
        "click .menu-button-rules-of-golf": "clickRulesOfGolf",
        "click .menu-button-explore-decisions": "clickExploreDecisions",
        "click .menu-button-videos": "clickVideos",
        "click .menu-button-my-decisions": "clickMyDecisions",
        "click .rolex-button": "clickRolex"
    },
    initialize: function () {
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        var self = this;
        self.templateFromFile("templates/menu.html", self.$el, null, function () {
            _.delay(self.callback, 10);
            _.delay(function () {
                if (window.useNativeAnimation && window.useNativeMenuAnimation) {
                    window["pluginsnativepagetransitions"]["executePendingTransition"]();
                }
                _.delay(function () {
                    window.menuStatus = "opened";
                }, window.menuTransitionDuration + 50);
            }, 100);
            logger("menu view drawing done");
        });
        return self;
    },
    clickOverview: function () {
        app.main.hideMenu();
        _.delay(function () {
            if ("#overview" !== window.location.hash) {
                window.location.hash = "#overview";
            }
        }, 50);
        return false;
    },
    clickSearchDecisions: function () {
        app.main.hideMenu();
        _.delay(function () {
            var weAreHere = ("#rulesAndDecisionsList" === window.location.hash);
            _.delay(app.main.showSearch, weAreHere ? 50 : 400);
            if (!weAreHere) {
                window.location.hash = "#rulesAndDecisionsList";
            }
            window.searchOn = true;
        }, 50);
        return false;
    },
    clickRulesOfGolf: function () {
        app.main.hideMenu();
        _.delay(function () {
            if ("#rulesList" !== window.location.hash) {
                window.location.hash = "#rulesList";
            }
        }, 50);
        return false;
    },
    clickExploreDecisions: function () {
        app.main.hideMenu();
        _.delay(function () {
            var weAreHere = ("#rulesAndDecisionsList" === window.location.hash);
            _.delay(app.main.showExploreDecisions, weAreHere ? 200 : 800);
            if (!weAreHere) {
                window.location.hash = "#rulesAndDecisionsList";
            }
            window.exploreDecisionsOn = true;
        }, 50);
        return false;
    },
    clickVideos: function () {
        app.main.hideMenu();
        _.delay(function () {
            if ("#videosList" !== window.location.hash) {
                window.location.hash = "#videosList";
            }
        }, 50);
        return false;
    },
    clickMyDecisions: function () {
        app.main.hideMenu();
        _.delay(function () {
            if ("#folders" !== window.location.hash) {
                window.location.hash = "#folders";
            }
        }, 50);
        return false;
    },
    clickRolex: function () {
        app.main.hideMenu();
        _.delay(function () {
            if ("#rolex" !== window.location.hash) {
                window.location.hash = "#rolex";
            }
        }, 50);
        return false;
    }
});