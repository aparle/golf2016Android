app.OverviewView = Backbone.View.extend({
    id: "overview-view-i",
    className: "full-screen-view",
    events: {
        "click .oli": "clickItem",
        "swiperight": "openMenu",
        "swipeRight": "openMenu"
    },
    initialize: function () {
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        var self = this,
            el,
            el2;
        logger("overview view", null, true);
        self.templateFromFile("pages/overview.html", self.$el, {}, function () {
            _.delay(function () {
                window.overviewIScroll = app.core.createIScroll("#overview-wrapper");
            }, 300);
            _.delay(self.callback, 150);
            logger("overview view drawing done");
        });
        return this;
    },
    _removeIScroll: function () {
        if (window.overviewIScroll) {
            window.overviewIScroll.destroy();
            window.overviewIScroll = null;
        }
    },
    clickItem: function (e) {
        var listItem = $(e.target).closest(".oli"),
            data = listItem.data("file");
        //logger(data, "clickItem", true);
        if (data) {
            var self = this;
            app.main.highlightListItemByAddition(listItem, function () {
                self._removeIScroll();
                _.delay(function () {
                    //spinnerDialogShow();
                    window.location.hash = "#page/" + encodeURIComponent(data);
                }, 30);
            });
        }
        return false;
    },
    openMenu: function () {
        app.main.showMenu();
        return false;
    },
    onOrientationChange: function () {
        _.delay(function () {
            app.core.refreshIScroll(window.overviewIScroll, 40);
        }, 300);
    }
});