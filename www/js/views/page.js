app.PageView = Backbone.View.extend({
    id: "page-view-i",
    className: "full-screen-view",
    initialize: function () {
        this.fileName = "";
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    render: function () {
        var self = this,
            el,
            el2;
        if (!self.fileName) {//emergency
            self.$el.parent().hide();
            self.remove();
            logger("page view abandoned");
            return;
        }
        logger(self.fileName, "page view", true);
        el2 = $("<div/>").attr("id", "page-content-wrapper").addClass("content-wrapper sides-padded").appendTo(self.$el);
        el = $("<div/>").attr("id", "page-content").appendTo(el2);
        self.templateFromFile("pages/" + self.fileName + ".html", el, {}, function () {
            var videos = app.main.prepareVideoTags(el, ".html5Video");
            _.delay(app.main.scaleVideoIFrame, 200);
            _.delay(function () {
                window.pageIScroll = app.core.createIScroll("#page-content-wrapper");
                if (videos.length > 0) {
                    app.main.playVideo(videos[0]);
                }
            }, 300);
            _.delay(self.callback, 150);
            logger("page view drawing done");
        });
        return this;
    },
    _removeIScroll: function () {
        if (window.pageIScroll) {
            window.pageIScroll.destroy();
            window.pageIScroll = null;
        }
    },
    onOrientationChange: function () {
        _.delay(function () {
            app.main.scaleVideoIFrame();
            app.core.refreshIScroll(window.pageIScroll, 40);
        }, 300);
    }
});