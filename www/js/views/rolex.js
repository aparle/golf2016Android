app.RolexView = Backbone.View.extend({
    id: "rolex-view-i",
    className: "full-screen-view",
    events: {
        "click img": "navigateToRolex",
        "swiperight": "openMenu",//jQuery
        "swipeRight": "openMenu",//Zepto
        "swipeleft": "goBack",//jQuery
        "swipeLeft": "goBack"//Zepto
    },
    initialize: function () {
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _url: "",
    _setUrl: function () {
        var w = $(window),
            ww = w.width(),
            wh = w.height();
        if (window.isPhoneView) {
            if (ww >= 568 || wh >= 568) {//height = 568
                this._url = "http://m.rolex.com/?cmpid=dw201401615";
            }
            else if (window.devicePixelRatio >= 2) {//scale = 2
                this._url = "http://m.rolex.com/?cmpid=dw201401613";
            }
            else {//default mobile
                this._url = "http://m.rolex.com/?cmpid=dw201401614";
            }
        } else {
            if (ww >= 768 || wh >= 768) {
                this._url = "http://www.rolex.com/magazine/sports-and-culture/golf.html?cmpid=dw201401611";
            }
            else if (ww >= 525 || wh >= 525) {
                this._url = "http://www.rolex.com/magazine/sports-and-culture/golf.html?cmpid=dw201401612";
            }
        }
    },
    _preCacheImages: function () {
        if (window.rolexPreCached) {
            return;
        }
        window.rolexPreCached = true;
        var holder = $("<div/>").css("display", "none").appendTo($("body"));
        _.each([
            "img/rolex/opd40/2048x1536.jpg",
            "img/rolex/opd40/1536x2048.jpg",
            "img/rolex/opd40/1242x2208.jpg"
        ], function (e) {
            $("<img/>").attr("src", e).appendTo(holder);
        });
    },
    _replaceImage: function () {
        var w = $(window),
            ww = w.width(),
            wh = w.height(),
            ratioScreen = ww / wh,
            src,
            isLandscape = $("body").hasClass("landscape");
        if (isLandscape) {
            src = "img/rolex/opd40/2048x1536.jpg";
        }
        else if (ratioScreen > 0.66) {//wider portrait
            src = "img/rolex/opd40/1536x2048.jpg";
        }
        else {//narrower portrait
            src = "img/rolex/opd40/1242x2208.jpg";
        }
        this.$el.html($("<img/>", {
            src: src
        }));
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        this._preCacheImages();
        this._replaceImage();
        this._setUrl();
        this.callback();
        logger("rolex view drawing done");
        return this;
    },
    _browserRef: {},
    navigateToRolex: function () {
        logger(this._url, "navigateToRolex, url");
        this._browserRef = cordova.InAppBrowser.open(this._url, "_blank", "hardwareback=no,location=yes,zoom=yes");
        return false;
    },
    openMenu: function () {
        app.main.showMenu();
        return false;
    },
    goBack: function () {
        app.main.goBackInHistoryStack();
        return false;
    },
    onOrientationChange: function () {
        this._replaceImage();
        this._setUrl();
    }
});