app.RuleSelectorView = Backbone.View.extend({
    events: {
        "touchstart": "selectRule",
        "touchmove": "selectRule"
    },
    _mainRules: {},
    _query1: function () {
        logger("rule selector, _query1");
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [main_rule_number] FROM [ZMAINRULE] ORDER BY [main_rule_number]",
            this._mainRules,
            null,
            function (res) {
                self._mainRules = res;
                self._render();
            }
        );
    },
    _render: function () {
        var self = this;
        this.templateFromFile("templates/rule-selector.html", self.$el, {
            mainRules: self._mainRules
        }, function () {
            _calculateHeight();
            logger("rule selector view drawing done");
        });
    },
    render: function () {
        this._query1();
        return this;
    },
    _selectedRule: "",
    selectRule: function (e) {
        var el = getElementFromUnderTouch(e),
            id = el
                ? el.id.replace("rs", "rsn").replace("rb", "rsn")//the latter fix is for the case of bullets
                : "";
        if (id && id !== this._selectedRule) {
            this._selectedRule = id;
            if (window.useIScroll && window.rulesAndDecisionsListIScroll) {
                window.rulesAndDecisionsListIScroll.scrollToElement("#" + id, 0);
            } else {//in case the iScroller is not present, we need to do this "manually"
                document.getElementById("rules-and-decisions-list-wrapper").scrollTop = document.getElementById(id).offsetTop;
            }
        }
        return false;
    }
});
var _calculateHeight = function () {
    var counter = 0,
        calculateHeight = function (fontSize) {
            var el = $("#rule-selector");
            if (el.length < 1) {
                return;//nothing to proceed with
            }
            var numbers = el.find(".rs");
            counter += 1;
            //logger(el.is(":visible"), "calc height, rule selector is visible");
            if (fontSize && parseInt(fontSize) > 0) {
                el.css("font-size", fontSize + "px");
            } else {
                el.css("font-size", "inherit");
                fontSize = parseInt(window.getComputedStyle(el[0]).fontSize, 10);
            }
            //logger(fontSize, "calc height, fontSize");
            var height = el.outerHeight(true),
                numbersCount = numbers.length,
                numbersFirst = numbers.first(),
                numbersLast = numbers.last(),
                numberHeight = getHeight(numbersFirst),
                bullets = el.find(".rsb"),
                padding = Math.round(((height * 0.9) - (numbersCount * numberHeight)) / (numbersCount * 2)),
                paddingString = padding + "px 0 " + padding + "px",
                outerPadding;
            if (padding < 0) {//short side
                el.find(".even").hide();//hide even numbers
                bullets.show();//bullets have to be shown before their height is measured
                var bulletFirst = bullets.first(),
                    bulletHeight = getHeight(bulletFirst),
                    bulletMarginDiff = Math.floor((numberHeight - bulletHeight) / 2),
                    bulletsMarginString = (padding + bulletMarginDiff - 1) + "px 0 " + (padding + bulletMarginDiff) + "px";
                bullets.css("margin", bulletsMarginString);
                numbers.css("padding", "0").css("margin", paddingString);
            } else {//long side
                el.find(".even").show();//show even numbers
                bullets.hide();
                numbers.css("margin", "0").css("padding", paddingString);
            }
            outerPadding = Math.round((height - getHeight(el.find("#rsw"))) / 2);//recalculate outer padding
            //logger(outerPadding, "calc height, outerPadding");
            if (outerPadding < 2 && counter < 15 && (!fontSize || (fontSize && fontSize >= 5))) {
                //rule selector is overflowing
                calculateHeight(fontSize - 1);//try with one pixel less
                return;
            }
            numbersFirst.css("padding-top", outerPadding + "px");
            numbersLast.css("padding-bottom", outerPadding + "px");
            /*
             logger(height, "calc height, height");
             logger(numberHeight, "calc height, numberHeight");
             logger(bulletHeight, "calc height, bulletHeight");
             logger(bulletMarginDiff, "calc height, bulletMarginDiff");
             logger(padding, "calc height, padding");
             //*/
            if (window.firstStart) {
                window.firstStart = false;
                _.delay(navigator.splashscreen.hide, 2000);
                _.delay(app.main.showMenu, 300);
            }
            _.delay(spinnerDialogHide, 100);
        };
    calculateHeight();
};