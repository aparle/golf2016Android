app.RuleView = Backbone.View.extend({
    id: "rule-view-i",
    className: "full-screen-view",
    events: {
        "click .d": "clickDecision",
        "click #phone-view-decisions-holder-i": "clickDecisionsHolder",
        "click #phone-view-media-holder-i": "clickMediaHolder",
        "swiperight": "rulePrevious",//jQuery
        "swipeRight": "rulePrevious",//Zepto
        "click .previous-item": "rulePrevious",
        "swipeleft": "ruleNext",//jQuery
        "swipeLeft": "ruleNext",//Zepto
        "click .next-item": "ruleNext"
    },
    initialize: function () {
        this.rute = "";
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _rule: {},
    _decisions: {},
    _media: {},
    _query1: function () {
        var sql,
            self = this;
        if (self.rute.indexOf("-") >= 0) {
            sql = "SELECT * FROM [ZSUBRULE] WHERE [ZRUTE] = '" + self.rute + "'";
        } else {
            sql = "SELECT * FROM [ZMAINRULE] WHERE [ZRULENUM] = '" + self.rute + "'";
        }
        queryAndProcessSqlResult(
            sql,
            this._rule,
            null,
            function (res) {
                self._rule = res[0];
                //title
                if (self._rule["ZMAINRULESTITLE"]) {
                    self._rule["ZMAINRULESTITLE"] = app.main.highlightFoundWords(self._rule["ZMAINRULESTITLE"]);
                }
                else if (self._rule["ZSUBRULESTITLE"]) {
                    self._rule["ZSUBRULESTITLE"] = app.main.highlightFoundWords(self._rule["ZSUBRULESTITLE"]);
                }
                //text
                if (self._rule["ZMAINRULESTXT"]) {
                    self._rule["ZMAINRULESTXT"] = app.main.highlightFoundWords(self._rule["ZMAINRULESTXT"]);
                    self._rule["ZMAINRULESTXT"] = app.main.convertLinks(self._rule["ZMAINRULESTXT"]);
                }
                else if (self._rule["ZSUBRULESTEXT"]) {
                    self._rule["ZSUBRULESTEXT"] = app.main.highlightFoundWords(self._rule["ZSUBRULESTEXT"]);
                    self._rule["ZSUBRULESTEXT"] = app.main.convertLinks(self._rule["ZSUBRULESTEXT"]);
                }
                //for sharing
                window.sharingSubject ="Rule:"+self._rule["ZRUTE"];
                //window.sharingSubject = self._rule["ZMAINRULESTITLE"] || self._rule["ZSUBRULESTITLE"];
                window.sharingMessage = app.main.prepareMessageForSharing(self._rule["ZMAINRULESTXT"] || self._rule["ZSUBRULESTEXT"]);
                self._query2();
            }
        );
    },
    _query2: function () {
        var self = this,
            ruteArr = self.rute.split("-");
        if (!ruteArr[1]) {
            ruteArr[1] = "";//special case, also the main rules can have decisions
        }
        queryAndProcessSqlResult(
            "SELECT [ZDECISIONTITLE], [ZRUTE], [main_rule_number], [sub_rule_number] " +
            "FROM [ZDECISION] " +
            "WHERE [ZMAINRULENUMBER] = '" + ruteArr[0] + "' AND [ZSUBRULENUM] = '" + ruteArr[1] + "' " +
            "ORDER BY [ZORDER]",
            this._decisions,
            null,
            function (res) {
                self._decisions = res;
                self._query3();
            }
        );
    },
    _query3: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZPICTURE], [ZPICTUREDESC], [ZVIDEOIMG], [ZVIDEOTITLE], [ZVIDEODESC], [ZRUTE] " +
            "FROM [ZMEDIA] " +
            "WHERE [ZRUTE] = '" + self.rute + "' " +
            "ORDER BY [ZORDER]",
            this._media,
            null,
            function (res) {
                self._media = res;
                self._render();
            }
        );
    },
    _render: function () {
        var self = this;
        if (!self._rule) {//emergency
            self.$el.parent().hide();
            self.remove();
            logger("rule view abandoned");
            return;
        }
        self.templateFromFile("templates/rule.html", self.$el, {
            rule: self._rule,
            decisions: self._decisions,
            media: self._media
        }, function () {
            _decisionsCount = _.size(self._decisions);
            _mediaCount = _.size(self._media);
            _setRuleContentWrappersHeights(self.$el, _decisionsCount, _mediaCount);
            app.main.highlightListItemByRetrieval();
            _.delay(function () {
                app.main.replaceImgBySvg(self.$el.find(".phone-view-holder-button"));
                app.main.replaceImgBySvg(self.$el.find("#footer-view"));
            }, 80);
            if (window.isPhoneView) {
                $("#decisions").addClass("holder close").appendTo("#phone-view-decisions-holder");
                $("#media").addClass("holder close").appendTo("#phone-view-media-holder");
            }
            _.delay(function () {
                window.ruleIScroll = app.core.createIScroll("#rule-content-wrapper");
                if (_mediaCount) {
                    app.main.truncateMediaDesc();
                }
                if (!window.isPhoneView) {
                    if (_decisionsCount) {
                        window.decisionsIScroll = app.core.createIScroll("#decisions-content-wrapper");
                    }
                    if (_mediaCount) {
                        window.mediaIScroll = app.core.createIScroll("#media-content-wrapper");
                    }
                }
            }, 200);
            if (_mediaCount) {
                app.main.attachMediaPopup("#media-content");
            }
            _.delay(spinnerDialogHide, 90);
            _.delay(self.callback, 150);
            logger("rule view drawing done");
        });
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        logger(this.rute, "rule view", true);
        if (this.rute) {
            this._query1();
        }
        return this;
    },
    _removeIScroll: function () {
        if (window.ruleIScroll) {
            window.ruleIScroll.destroy();
            window.ruleIScroll = null;
        }
        if (!window.isPhoneView) {
            if (_decisionsCount && window.decisionsIScroll) {
                window.decisionsIScroll.destroy();
                window.decisionsIScroll = null;
            }
            if (_mediaCount && window.mediaIScroll) {
                window.mediaIScroll.destroy();
                window.mediaIScroll = null;
            }
        }
    },
    clickDecision: function (e) {
        //app.core.logEvent(e);
        var listItem = $(e.target).closest(".dli"),
            data = listItem.data("rute");
        //logger(data, "clickDecision", true);
        if (data) {
            var self = this;
            app.main.highlightListItemByAddition(listItem, function () {
                self._removeIScroll();
                _.delay(function () {
                    app.main.setTemporaryOptionsForLateralMovements();
                    //spinnerDialogShow();
                    window.location.hash = "#decision/" + encodeURIComponent(data);
                }, 30);
            });
        }
        return false;
    },
    clickDecisionsHolder: function () {
        app.main.toggleHolder(this, $("#phone-view-decisions-holder-i"), $("#decisions"));
        return false;
    },
    clickMediaHolder: function () {
        app.main.toggleHolder(this, $("#phone-view-media-holder-i"), $("#media"));
        return false;
    },
    rulePrevious: function () {
        window.rulesAndSubRulesSequence.setCur(_.indexOf(window.rulesAndSubRulesSequence, this.rute));
        var rule = window.rulesAndSubRulesSequence.prev();
        if (rule) {
            app.main.highlightClickedButton(".previous-item");
            this._removeIScroll();
            _.delay(function () {
                window.historyBackDetected = true;
                app.main.setTemporaryOptionsForLateralMovements();
                //spinnerDialogShow();
                app.router.navigateCustom("#rule/" + encodeURIComponent(rule), {trigger: true, replace: true});
            }, 30);
        }
        return false;
    },
    ruleNext: function () {
        window.rulesAndSubRulesSequence.setCur(_.indexOf(window.rulesAndSubRulesSequence, this.rute));
        var rule = window.rulesAndSubRulesSequence.next();
        if (rule) {
            app.main.highlightClickedButton(".next-item");
            this._removeIScroll();
            _.delay(function () {
                app.main.setTemporaryOptionsForLateralMovements();
                //spinnerDialogShow();
                app.router.navigateCustom("#rule/" + encodeURIComponent(rule), {trigger: true, replace: true});
            }, 30);
        }
        return false;
    },
    onOrientationChange: function () {
        _.delay(function (view) {
            _setRuleContentWrappersHeights(view.$el, _decisionsCount, _mediaCount);
            app.core.refreshIScroll(window.ruleIScroll, 50);
            if (_mediaCount) {
                app.main.truncateMediaDesc();
            }
            if (!window.isPhoneView) {
                if (_decisionsCount) {
                    app.core.refreshIScroll(window.decisionsIScroll, 60);
                }
                if (_mediaCount) {
                    app.core.refreshIScroll(window.mediaIScroll, 70);//more time for images to load
                }
            }
        }, 100, this);
    }
});
var _decisionsCount = 0,
    _mediaCount = 0,
    _setRuleContentWrappersHeights = function (viewEl, decisionsCount, mediaCount) {
        var ruleViewWithoutFooter = $("#rule-view-i2");
        viewEl = viewEl || ruleViewWithoutFooter;
        var rule = viewEl.find("#rule"),
            ruleMarginTop = Math.round(parseFloat(rule.css("margin-top").replace("px", ""))),
            ruleTitleHeight = Math.round(viewEl.find("#rule-title").outerHeight()),
            height = Math.round(parseFloat(getHeight(ruleViewWithoutFooter))) - (ruleTitleHeight + (2 * ruleMarginTop));
        viewEl.find("#rule-content-wrapper").height(height);
        if (window.isPhoneView) {
            $("#rule-title-bg").height(ruleTitleHeight + ruleMarginTop);
            return;//we don't need the rest in the phone view
        }
        var borderWidth = 1,
            sideHeight = height,
            decisions = viewEl.find("#decisions"),
            media,
            mediaTitleHeight,
            numberOfBoxes = 0,
            sideScrollHeight;
        if (decisionsCount) {
            numberOfBoxes += 1;
        }
        if (mediaCount) {
            mediaTitleHeight = Math.round(viewEl.find("#media-title").outerHeight());
            media = viewEl.find("#media");
            sideHeight -= (mediaTitleHeight + Math.round(parseFloat(media.css("margin-top").replace("px", ""))));
            numberOfBoxes += 1;
        }
        if (numberOfBoxes) {
            sideHeight = Math.floor(sideHeight / numberOfBoxes);
            if (decisionsCount) {
                decisions.height(ruleTitleHeight + sideHeight);
            }
            if (mediaCount) {
                media.height(mediaTitleHeight + sideHeight);
            }
            sideScrollHeight = sideHeight - (2 * borderWidth);
            viewEl.find(".scroll-content-border").height(sideScrollHeight);
            viewEl.find(".side-content-wrapper").height(sideScrollHeight);
        }
    };
