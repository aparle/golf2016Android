app.RulesAndDecisionsListView = Backbone.View.extend({
    id: "rules-and-decisions-list-view-i",
    className: "full-screen-view",
    events: {
        "click .r": "clickRule",
        "click .d": "clickDecision",
        "swiperight": "openMenu",
        "swipeRight": "openMenu"
    },
    initialize: function () {
        this.rute = "";
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _mainRules: {},
    _subRules: {},
    _decisions: {},
    _query1: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZMAINRULESTITLE], [main_rule_number] FROM [ZMAINRULE] ORDER BY [main_rule_number]",
            this._mainRules,
            "main_rule_number",
            function (res) {
                self._mainRules = res;
                self._query2();
            }
        );
    },
    _query2: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZSUBRULESTITLE], [ZRUTE], [main_rule_number], [sub_rule_number] FROM [ZSUBRULE] ORDER BY [main_rule_number], [sub_rule_number]",
            this._subRules,
            "ZRUTE",
            function (res) {
                self._subRules = res;
                self._query3();
            }
        );
    },
    _query3: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZDECISIONTITLE], [ZRUTE], [main_rule_number], [sub_rule_number] FROM [ZDECISION] ORDER BY [ZORDER]",
            this._decisions,
            "ZRUTE",
            function (res) {
                self._decisions = res;
                self._render();
            }
        );
    },
    _render: function () {
        var self = this;
        window.cacheMainRules = self._mainRules;//cache for further usage
        window.cacheSubRules = self._subRules;
        window.cacheDecisions = self._decisions;
        this.templateFromFile("templates/rules-and-decisions-list.html", self.$el, {
            mainRules: self._mainRules,
            subRules: self._subRules,
            decisions: self._decisions
        }, function () {
            app.main.highlightListItemByRetrieval();
            _.delay(function () {
                window.rulesAndDecisionsListIScroll = app.core.createIScroll("#rules-and-decisions-list-wrapper", true, false, window.useIScrollSelective);
                //window.rulesListIScroll.enableStickyHeaders(".rsn");
            }, 200);
            self.assign((new app.RuleSelectorView()), "#rule-selector");
            _.delay(self.callback, 150);
            logger("rules and decisions list view drawing done");
        });
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        this._query1();
        return this;
    },
    _click: _.debounce(function (e, type) {
        //app.core.logEvent(e);
        if (type !== "rule" && type !== "decision") {
            logger(type, "rules and decisions list, _click, incorrect type supplied", true);
            return;
        }
        var listItem = $(e.target).closest(".rli"),
            data = listItem.data("rute");
        if (data) {
            app.main.highlightListItemByAddition(listItem, function () {
                _.delay(function () {
                    //spinnerDialogShow();
                    window.location.hash = "#" + type + "/" + encodeURIComponent(data);
                }, 100);
            });
        }
    }, 300, true),
    clickRule: function (e) {
        this._click(e, "rule");
        return false;
    },
    clickDecision: function (e) {
        this._click(e, "decision");
        return false;
    },
    openMenu: function () {
        app.main.showMenu();
        return false;
    },
    onOrientationChange: function () {
        app.core.refreshIScroll(window.rulesAndDecisionsListIScroll, 250);
        _.delay(_calculateHeight, 300);
    }
});