app.RulesListView = Backbone.View.extend({
    id: "rules-list-view-i",
    className: "full-screen-view",
    events: {
        "click #rl-e1": "clickEtiquette",
        "click #rl-e2": "clickDefinitions",
        "click .r": "clickRule",
        "click .a": "clickAppendix",
        "swiperight": "openMenu",
        "swipeRight": "openMenu"
    },
    initialize: function () {
        this.rute = "";
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _mainRules: {},
    _appendices: {},
    _query1: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZMAINRULESTITLE], [main_rule_number] FROM [ZMAINRULE] ORDER BY [main_rule_number]",
            this._mainRules,
            "main_rule_number",
            function (res) {
                self._mainRules = res;
                self._query2();
            }
        );
    },
    _query2: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZAPPENDIXTITLE], [ZAPPENDIXSUBTITLE], [ZORDER], [Z_PK] FROM [ZAPPENDIX] ORDER BY [ZORDER]",
            this._appendices,
            "ZORDER",
            function (res) {
                self._appendices = res;
                self._render();
            }
        );
    },
    _render: function () {
        var self = this,
            listSections = {
                r1: "The Game",
                r4: "Clubs and the Ball",
                r6: "Player's Responsibilities",
                r10: "Order of Play",
                r11: "Teeing Ground",
                r12: "Playing the Ball",
                r16: "The Putting Green",
                r18: "Ball Moved, Deflected or Stopped",
                r20: "Relief Situations and Procedure",
                r29: "Other Forms of Play",
                r33: "Administration",
                r35: "Miscellaneous"
            };
        //count the rules and decisions for the log
        //logger(_.size(self._mainRules), "self._mainRules, count");
        //logger(_.size(self._appendices), "self._appendices, count");
        this.templateFromFile("templates/rules-list.html", self.$el, {
            mainRules: self._mainRules,
            appendices: self._appendices,
            listSections: listSections
        }, function () {
            app.main.highlightListItemByRetrieval();
            _.delay(function () {
                window.rulesListIScroll = app.core.createIScroll("#rules-list-wrapper");
                //window.rulesListIScroll.enableStickyHeaders(".rsn");
            }, 200);
            _.delay(self.callback, 150);
            logger("rules list view drawing done");
        });
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        this._query1();
        return this;
    },
    _click: _.debounce(function (e, type) {
        if (type !== "etiquette" && type !== "definitions" && type !== "subRulesList" && type !== "appendix") {
            logger(type, "rules list, _click, incorrect type supplied", true);
            return;
        }
        var listItem = $(e.target).closest(".rli"),
            data = listItem.data("rute");
        if (data) {
            app.main.highlightListItemByAddition(listItem, function () {
                _.delay(function () {
                    //spinnerDialogShow();
                    window.location.hash = "#" + type + "/" + encodeURIComponent(data);
                }, 30);
            });
        } else {
            app.main.highlightListItemByAddition(listItem, function () {
                _.delay(function () {
                    //spinnerDialogShow();
                    window.location.hash = "#" + type;
                }, 30);
            });
        }
    }, 300, true),
    clickEtiquette: function (e) {
        this._click(e, "etiquette");
        return false;
    },
    clickDefinitions: function (e) {
        this._click(e, "definitions");
        return false;
    },
    clickRule: function (e) {
        this._click(e, "subRulesList");
        return false;
    },
    clickAppendix: function (e) {
        this._click(e, "appendix");
        return false;
    },
    openMenu: function () {
        app.main.showMenu();
        return false;
    },
    onOrientationChange: function () {
        app.core.refreshIScroll(window.rulesListIScroll, 250);
    }
});