app.SearchView = Backbone.View.extend({
    id: "search-view-i",
    events: {
        "keyup #search-input": "keyUpSearch",
        "submit #search-form": "submitForm",
        "click .clear-button": "clickClear"
    },
    render: function () {
        var self = this;
        self.templateFromFile("templates/search.html", self.$el, {}, function () {
            logger("search view drawing done");
        });
        return self;
    },
    _cleanSearchWords: function (str) {
        var arr1 = str.toLowerCase().split(" "),
            arr2 = [],
            blacklist = ["span", "div", "strong", "textformat", "blockindent", "class", "href", "rule", "decision", "appendix", "definition"],
            isBlacklisted = function (s1) {
                return !!_.find(blacklist, function (s2) {
                    return (s2.indexOf(s1) > -1);
                });
            };
        _.each(arr1, function (s) {
            if (typeof s === "string" && s !== "" && s.length > 2 && !isBlacklisted(s)) {
                arr2.push(s);
            }
        });
        arr2.sort();
        arr2 = _.uniq(arr2, true);
        return arr2.join(" ");
    },
    _constructSearchQuery: function (str) {
        var strArr = str.split(" "),
            fields = function (fieldsArr) {
                return _.map(strArr, function (s) {
                    return "(" + _.map(fieldsArr, function (f) {
                            return f + " LIKE \"%" + s + "%\"";
                        }).join(" OR ") + ")";
                }).join(" AND ");
            };
        return [
            "SELECT ZRUTE FROM ZDECISION WHERE", fields(["ZDECISIONA", "ZDECISIONQ", "ZDECISIONTITLE"]),
            "UNION SELECT ZRULENUM FROM ZMAINRULE WHERE", fields(["ZMAINRULESTITLE", "ZMAINRULESTXT"]),
            "UNION SELECT ZRUTE FROM ZSUBRULE WHERE", fields(["ZSUBRULESTITLE", "ZSUBRULESTEXT"]),
            "ORDER BY 1"
        ].join(" ");
    },
    _displayFound: function (resArr, str) {
        if (!$.isArray(resArr)) {
            resArr = [];
        }
        var wrapper = $("#rules-and-decisions-list-wrapper"),
            list = $("#rules-and-decisions-list"),
            selector = $("#rule-selector");
        if (resArr.length > 0) {//display found
            selector.hide();
            wrapper.addClass("no-selector");
            if (window.useIScroll && window.rulesAndDecisionsListIScroll) {
                window.rulesAndDecisionsListIScroll.scrollTo(0, 0);
            } else {//in case the iScroller is not present, we need to do this "manually"
                wrapper.scrollTop(0);
                //document.getElementById("rules-and-decisions-list-wrapper").scrollTop = 0;
            }
            list.hide();
            list.children().each(function (ignore, e) {
                if (resArr.indexOf(e.id) === -1) {
                    $(e).addClass("nv");
                } else {
                    $(e).removeClass("nv");
                }
            });
            this._highlightFoundWordsInTheList(str);
        } else {//display all
            wrapper.removeClass("no-selector");
            selector.show();
            list.hide();
            list.children().each(function (ignore, e) {
                $(e).removeClass("nv");
            });
            this._highlightFoundWordsInTheList("");
        }
        list.show();
        app.rulesAndDecisionsListView.onOrientationChange();
        $("#search-result-count").html(resArr.length || "0");
    },
    _highlightFoundWordsInTheList: function (searchStr) {
        var list = $("#rules-and-decisions-list");
        list.find("span.hlw").contents().unwrap();//unhighlight found words
        if (searchStr === "" || searchStr === undefined) {
            return;
        }
        var el = list.find("div.rli").not(".nv");
        if (el.length < 1) {
            return;
        }
        var $e,
            htmlArr,
            div = "</div>",
            rnTag,
            replaceStr;
        el.each(function (ignore, e) {
            $e = $(e);
            htmlArr = trim($e.html()).split(div);
            rnTag = htmlArr.shift() + div;
            replaceStr = htmlArr.join(div);
            replaceStr = replaceStr.replace(
                new RegExp("(" + searchStr.split(" ").join("|") + ")(?![^<]*>)", "img"),
                "<span class='hlw'>$1</span>");
            $e.html(rnTag + replaceStr);
        });
    },
    keyUpSearch: _.debounce(function () {
        var self = this,
            str = $("#search-input").val();
        if (typeof str !== "string") {
            return;
        }
        str = trim(str);
        window.searchInput = str;//store for further usage, e.g. return to the search view
        str = self._cleanSearchWords(str);
        window.searchInputCleaned = str;//ditto
        if (str.length < 3) {
            self._displayFound([]);
            return;
        }
        queryAndProcessSqlResult(
            self._constructSearchQuery(str),
            {}, null,
            function (res) {
                var resArr = [],
                    rute;
                _.each(res, function (value, key) {
                    rute = app.main.makeDecisionIdFromNumberText(value["ZRUTE"]);
                    if (rute.indexOf("__") !== -1) {//decision
                        rute = "rdl-d" + rute;
                    } else if (rute.indexOf("-") !== -1) {//sub-rule
                        rute = "rdl-sr" + rute;
                    } else {//main rule
                        rute = "rdl-r" + rute;
                    }
                    resArr[key] = rute;
                });
                self._displayFound(resArr, str);
            }
        );
    }, 1000),
    submitForm: function () {
        $("#search-form").find(".clear-button").focus();
        return false;
    },
    clickClear: function () {
        var self = this;
        app.main.highlightClickedButton($("#search-form").find(".clear-button"));
        _.delay(function () {
            window.searchInput = "";
            window.searchInputCleaned = "";
            $("#search-input").val("");
            self._displayFound([]);
            return false;
        }, 30);
    }
});