app.SubRulesListView = Backbone.View.extend({
    id: "sub-rules-list-view-i",
    className: "full-screen-view",
    events: {
        "click .r": "clickRule",
        "swiperight": "rulePrevious",//jQuery
        "swipeRight": "rulePrevious",//Zepto
        "click .previous-item": "rulePrevious",
        "swipeleft": "ruleNext",//jQuery
        "swipeLeft": "ruleNext",//Zepto
        "click .next-item": "ruleNext"
    },
    initialize: function () {
        this.mainRuleId = 0;
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _mainRule: {},
    _subRules: {},
    _query1: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZMAINRULESTITLE], [main_rule_number] FROM [ZMAINRULE] WHERE [main_rule_number] = " + this.mainRuleId,
            this._mainRule,
            null,
            function (res) {
                self._mainRule = res[0];
                self._query2();
            }
        );
    },
    _query2: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZSUBRULESTITLE], [ZRUTE], [main_rule_number], [sub_rule_number] FROM [ZSUBRULE] WHERE [main_rule_number] = " + this.mainRuleId + " ORDER BY [main_rule_number], [sub_rule_number]",
            this._subRules,
            "ZRUTE",
            function (res) {
                self._subRules = res;
                self._render();
            }
        );
    },
    _render: function () {
        var self = this;
        this.templateFromFile("templates/sub-rules-list.html", self.$el, {
            mainRule: self._mainRule,
            subRules: self._subRules
        }, function () {
            app.main.highlightListItemByRetrieval();
            _.delay(function () {
                app.main.replaceImgBySvg(self.$el.find("#footer-view"));
            }, 80);
            _.delay(function () {
                window.subRulesListIScroll = app.core.createIScroll("#sub-rules-list-wrapper");
            }, 200);
            _.delay(self.callback, 150);
            logger("sub rules list view drawing done");
        });
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        this._query1();
        return this;
    },
    _removeIScroll: function () {
        if (window.subRulesListIScroll) {
            window.subRulesListIScroll.destroy();
            window.subRulesListIScroll = null;
        }
    },
    _click: _.debounce(function (e, type) {
        if (type !== "rule") {
            logger(type, "sub rules list, _click, incorrect type supplied", true);
            return;
        }
        var listItem = $(e.target).closest(".rli"),
            data = listItem.data("rute");
        if (data) {
            app.main.highlightListItemByAddition(listItem, function () {
                _.delay(function () {
                    //spinnerDialogShow();
                    window.location.hash = "#" + type + "/" + encodeURIComponent(data);
                }, 30);
            });
        }
    }, 300, true),
    clickRule: function (e) {
        this._click(e, "rule");
        return false;
    },
    rulePrevious: function () {
        window.rulesSequence.setCur(_.indexOf(window.rulesSequence, this.mainRuleId));
        var rule = window.rulesSequence.prev();
        if (rule) {
            app.main.highlightClickedButton(".previous-item");
            this._removeIScroll();
            _.delay(function () {
                window.historyBackDetected = true;
                app.main.setTemporaryOptionsForLateralMovements();
                //spinnerDialogShow();
                app.router.navigateCustom("#subRulesList/" + encodeURIComponent(rule), {
                    trigger: true,
                    replace: true
                });
            }, 30);
        }
        return false;
    },
    ruleNext: function () {
        window.rulesSequence.setCur(_.indexOf(window.rulesSequence, this.mainRuleId));
        var rule = window.rulesSequence.next();
        if (rule) {
            app.main.highlightClickedButton(".next-item");
            this._removeIScroll();
            _.delay(function () {
                app.main.setTemporaryOptionsForLateralMovements();
                //spinnerDialogShow();
                app.router.navigateCustom("#subRulesList/" + encodeURIComponent(rule), {
                    trigger: true,
                    replace: true
                });
            }, 30);
        }
        return false;
    },
    onOrientationChange: function () {
        app.core.refreshIScroll(window.subRulesListIScroll, 250);
    }
});