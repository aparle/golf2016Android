app.VideosListView = Backbone.View.extend({
    id: "videos-list-view-i",
    events: {
        "swiperight": "openMenu",
        "swipeRight": "openMenu"
    },
    initialize: function () {
        this.callback = function () {
            //this could be replaced by an external callback
        };
        return this;
    },
    _videos: {},
    _query1: function () {
        var self = this;
        queryAndProcessSqlResult(
            "SELECT [ZPICTURE], [ZPICTUREDESC], [ZVIDEOIMG], [ZVIDEOTITLE], [ZVIDEODESC], [ZRUTE] " +
            "FROM [ZMEDIA] " +
            "WHERE [ZVIDEOIMG] IS NOT NULL AND [ZVIDEOIMG] <> ''" +
            "ORDER BY [ZORDER]",
            this._videos,
            null,
            function (res) {
                self._videos = res;
                self._render();
            }
        );
    },
    _render: function () {
        var self = this;
        this.templateFromFile("templates/videos-list.html", self.$el, {
            videos: self._videos
        }, function () {
            _.delay(function () {
                //app.main.truncateMediaDesc();
                app.main.attachMediaPopup("#videos-list");
            }, 100);
            _.delay(function () {
                window.videosListIScroll = app.core.createIScroll("#videos-list-wrapper");
            }, 200);
            _.delay(self.callback, 150);
            logger("videos list view drawing done");
        });
    },
    render: function () {
        if ($.isFunction(this.$el.hammer)) {
            this.$el.hammer();
        }
        this._query1();
        return this;
    },
    _removeIScroll: function () {
        if (window.videosListIScroll) {
            window.videosListIScroll.destroy();
            window.videosListIScroll = null;
        }
    },
    openMenu: function () {
        app.main.showMenu();
        return false;
    },
    onOrientationChange: function () {
        _.delay(function () {
            //app.main.truncateMediaDesc();
            app.core.refreshIScroll(window.videosListIScroll, 40);
        }, 300);
    }
});